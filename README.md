# Richard Strauss - *Der Abend* - transcription for soprano, children choir, harp, piano, 2 solo instruments and live-electronics

This is the repository of a transcription of *Der Abend* from Richard Strauss' *Zwei Ges&auml;nge* (1897),
originally for a cappella sixteen part choir (4 part SATB), for the following instrumentation:

* children choir
* coloratura soprano (?)
* harp
* piano
* solo instrument 1 (?)
* solo instrument 2 (?)
* live-electronics

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Strauss - Der Abend - Transcription</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/nicb/Strauss-Der_Abend-transcription" property="cc:attributionName" rel="cc:attributionURL">Nicola Bernardini</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/nicb/Strauss-Der_Abend-transcription" rel="dct:source">https://gitlab.com/nicb/Strauss-Der_Abend-transcription</a>.



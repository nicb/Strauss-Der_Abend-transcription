sopranofirstpart = \relative c''' {
  \global
  \autoBeamOff
  s1 \bar ""
  R1*2
  r2 g2~^\markup { \dynamic "p" \italic "(vibrato sempre leggero)" } |
  g4. fis8 e4 b8 d8  |
  g1~g4 r4 r2 |
  R1*3
  r2 g2~g4. f8 es4 bes8 des8 g1~g4 r4 r2 r1
  %
  R1*2
  %
  % bar 17
  %
  r2 g2^\p~ g4. f8 es4 a,8 c8 g'1~g4 r4 r2
  %
  R1*4
  %
  % bar 25
  %
  r2 a2^\pp~ |
  a4. g8 fis4 d8 e8 |
  fis1~ |
  fis1^\markup { \italic "cresc." \right-align { \dynamic "f" } }\breathe
  a2 g4 g4 |
  fis2 e4 e4 |
  d1(^\markup { \italic "dim." } |
  c2) r2 |
  %
  % bar 33
  %
  r2 d2^\markup { \dynamic "f" \italic "dim." }~ |
  d2 d4 r4 \bar "||"  % 34
}

sopranosecondpart = \relative c'' {
  \key bes \major
  bes4\(^\markup { \dynamic "p" \italic "con tenerezza espressiva" } c8 d8 es2~ |
  es4 d4 f4 es4 |
  d2 c4\) bes4~ |
  bes8 d8 \autoBeamOn g,4(~ \tuplet 3/2 { g8 f8 g8 } \tuplet 3/2 { e8 f8 g8 } |
  \tuplet 3/2 { a8 g a } \tuplet 3/2 { f8 g a } bes8 d8 f4~ |
  f4 g4) f4 es4 |
  d1 |
  \autoBeamOff
  r2 es,4\(^\markup { \center-align \italic "con tenerezza espressiva" } f8 g8 |
  a2. g4 |
  bes4 a4 g2 |
  f4\) e4~e8 g8 c4~ |
  \autoBeamOn
  \tuplet 3/2 { c8( bes c } \tuplet 3/2 { a8 bes c } \tuplet 3/2 { d8 c d } \tuplet 3/2 { bes8 c d } |
  %   v-- this note is a g in the original score
  es8 d8 bes2 c4) |
  bes4 aes4\breathe g2^\mf^\> |
  \autoBeamOff
  c4^\! r4 aes4^\p bes8 c8 |
  \autoBeamOn
  \tuplet 3/2 { des8( es f } \tuplet 3/2 { es8 des c } b2~ |
  \autoBeamOff
  b8) d8 c4(~ \autoBeamOn \tuplet 3/2 { c8 bes c } \tuplet 3/2 { aes8 bes c } |
  \tuplet 3/2 { d8 c d } \tuplet 3/2 { bes8 c d) } es2 |
  e4.^\pp g8 bes,2(~ |
  \tuplet 3/2 { bes8 a bes } \tuplet 3/2 { c8 bes a } \tuplet 3/2 { g8 bes a) } \tuplet 3/2 { g8( f e) } |
  es2 r2 |
  R1*2 |
  %
  % bar 58
  %
  \autoBeamOff
  c'8^\p es8 \autoBeamOn a,4(~ \tuplet 3/2 { a8 g a } \tuplet 3/2 { f8 g a } |
  bes2~ bes8 d8) f8 r8 |
  \autoBeamOff
  es8^\p g \autoBeamOn c,4(~ c8 es8) a,4~ |
  \autoBeamOff
  a8 bes c4 r8 a c d | % 61
}

sopranothirdpart = \relative c'' {
  dis4 e2.~ | % 62
  e2 r8 a,^\< cis d |
  e4^\! fis2.^\markup { \right-align \italic "cresc." } |
  %
  % bar 65
  %
  r4 b,2^\p e,4 |
  r1^\markup { \right-align \italic "con espressione" } |
  gis2 gis4 r4 |
  e4\( fis8 gis8 a2~ |
  a4 gis b a |
  gis2 fis4^\) e~ |
  e8 gis \autoBeamOn cis4(~ \tuplet 3/2 { cis8 b cis } \tuplet 3/2 { a b cis } |
  \tuplet 3/2 { dis8 cis dis } \tuplet 3/2 { b8 cis dis } e8 dis b4~ |
  b4 cis) b a |
  gis1^\> |
  r1^\! |
  %
  % bar 76
  %
  R1*3
}

sopranofourthpart = \relative c'' {
  r2 r4 r8 g8^\p |
  bes4. g8^\> bes4. d8 |
  f2^\pp es2 |
  r1 |
  \autoBeamOff
  r2 es,2~^\p |
  es4 es es es8 es8 |
  f1 |
  e4 r4 r2 |
  R1*3
  %
  % bar 90
  %
  r2 f'2(^\p~ |
  \autoBeamOn
  \tuplet 3/2 { f8 g es } f4~ \tuplet 3/2 { f8 es c^\< } des4~ |
  \tuplet 3/2 { des8^\! es c } des4~ \tuplet 3/2 { des8 c ces } bes4 |
  \tuplet 3/2 { bes8 c! aes } bes4~^\> \tuplet 3/2 { bes8^\! aes f } g4~ |
  \tuplet 3/2 { g8 aes f) } \autoBeamOff g8^\markup { \center-align \italic "dim." } g8 g8 f4 fes8 |
  es1^\pp~ |
  es1~^\>  |
  es4^\! r4 r2 |
  r1 \bar "||" % 98
}

sopranofifthpart = \relative c'' {
  \key g \major
  R1*10
  bes2^\markup { \dynamic mf \italic "con espressione" } bes4^\< aes |
  f2^\!^\> g2^\! |
  r2 f'2~^\markup { \dynamic mf \italic "con espressione" } |
  f2 es4 bes |
  des2^( c2^\< |
  es2\! des2^\> |
  c1)^\!^\markup { \italic "dim." } |
  bes1 |
  g'1^\pp |
  b,2. b4 |
  b1 |
  cis2 dis2 |
  e1 |
  %
  R1*2
  %
  % bar 124
  %
  dis,1^\p |
  e2 r2 |
  \autoBeamOn
  e2.( fis8 gis8 |
  a1~ |
  a2.^\< cis8^\! b8 |
  a2) gis4 r4 |
  %
  R1*3
  %
  % bar 133
  %
  d1^\pp |
  d1     |
  d1(~   |
  d1     |
  dis1   |
  d!1)   |
  cis1   |
  %
  R1*3
  %
  % bar 143
  %
  dis'1^\markup { \italic "marc." }  |
  dis2 b2^\p |
  \autoBeamOff
  gis4.( fis8 e4) gis8 a8 |
  b2. gis4^\p |
  b4.^\accent b8 b2 |
  b1^\pp |
  %
  R1*2
  %
  gis'4.^\p( fis8) e4 \autoBeamOn gis,8( a8) |
  b1^\pp~ |
  b1^\markup { \right-align \italic "dim." }
  e,2 r2 |
  gis1-\ppp   | % 155
  gis1        | % 156
  gis1~       | % 157
  gis1~       | % 158
  gis1^\fermata \bar "|." % 159
}

soprano_text_first_part = \lyricmode {
  Sen -- ke strah -- len der Gott!
  Sen -- ke strah -- len der Gott!
  Sen -- ke strah -- len der Gott!
  Sen -- ke strah -- len der Gott!
  Sen -- ke den Wa -- gen hi -- nab!
  Sie -- he,
}

soprano_text_second_part = \lyricmode {
  wer aus des Meer's kry -- stall -- ner Wo -- gen lieb -- lich lä -- cheind dir winkt!
  wer aus des Meer's kry -- stall -- ner Wo -- gen lieb -- lich lä -- cheind dir
  Sie -- he, wer aus des Mee -- res Wo -- ge lieb -- lich lä -- cheind winkt.
  lieb -- lich lä -- cheind, lieb -- lich lä -- cheind dir winkt!
  Er -- kennt dein
}

soprano_text_third_part = \lyricmode {
  Herz sie?
  Er -- kennt dein Herz sie?
  The -- tys.
  Sie -- he, wer aus des Meer's kry -- stall -- ner Wo -- ge lieb -- lich lä -- cheind dir winkt.
}

soprano_text_fourth_part = \lyricmode {
  den Zaum er -- greift Cu -- pi -- do.
  Stil -- le hal -- ten die Ro -- se.
  trin -- ken die küh -- len -- de Fluth.
}

soprano_text_fifth_part = \lyricmode {
  Ru -- het und lie -- bet!
  Ru -- het und lie -- bet!
  Phö -- bus, der lie -- ben -- de ruht.
  Ru -- het!
  Lie -- bet!
  Phő -- bus,
  Phő -- bus,
  Phő -- bus, der strah -- len -- de ruht.
  der strah -- len -- de ruht.
  Ru -- het und lie -- bet!
  Phö -- bus ruht.
}

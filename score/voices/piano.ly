piano_RHa = {
  \global
  \once \override NoteHead.style = #'harmonic
  <g'' des'' d'' es'' des''' d''' es''' g'''>1_\markup {
    \tiny
    \center-align {
      \column {
       \line { \italic { (abbassare i tasti } }
       \vspace #-0.5
       \line { \italic { silenziosamente) } }
       }
     }
  }
  \bar ""
  g''1\laissezVibrer-\p |
  R1*2
  d''1-\p\laissezVibrer |
  R1*3
  des''1-\p\laissezVibrer |
  R1*6
  <c'' g''>1-\p\laissezVibrer |
  R1*1
  a'1-\p\laissezVibrer |
  R1*1
  aes'1-\p\laissezVibrer | % 19
  R1*1
  es''1-\p\laissezVibrer |
  R1*3
  a''1-\p\laissezVibrer |
  R1*3
  \arpeggioArrowUp
  <fis' d'' fis'' a''>2-\f\arpeggio <g' c'' g''>4\tenuto <g' e'' g''>\tenuto | % 29
  <fis' a' d'' fis''>2 <g' c'' e''>4\tenuto <c'' e''>4\tenuto |
  <<
    { \voiceOne
      <c'' d''>1\tenuto-\markup { \left-align \italic "dim." }^\(~ | % 31
      <c'' d''>1~ | % 32
      <c'' d''>1~ | % 33
      <c'' d''~>1 \bar "||" \key bes \major % 34
    }
    \new Voice
    { \voiceTwo
      <fis' a'>1\tenuto  |                                % 31
      <fis' a'>2\accent\tenuto-\ff g'4\tenuto g'\tenuto | % 32
      fis'2\tenuto e'4\tenuto e'\tenuto                 | % 33
      d'1\tenuto                                        | % 34
    }
  >>
  <<
    { \voiceOne
      <bes' d''>4^\)-\markup { \dynamic p \italic "con tenerezza espressiva" }\( c''8 d'' es''2~ | % 35
      es''4 d'' f'' es''  | % 36
      d''2 c''4\) bes'4~  | % 37
      bes'8 r8 g'4\(~ g' e' | % 38
      a'4 f' bes'8 d'' f''4~ | % 39
%     a'4 f' \tuplet 5/4 { bes'16_( c'' d'' es'' e'' } f''4~ | % 39
      f''4 g''\) f'' es'' | % 40
    }
    \new Voice
    { \voiceTwo
      s2 bes'4. bes'8     | % 35
      bes'4 bes' b' c''   | % 36
      bes'2.\( c'4\)      | % 37
      r1                  | % 38
      es'!4. es'8 d'2\(~  | % 39
      d'8 f' bes'4 c''\) a' | % 40
%     d'16( es' f' g' bes'4) c''\) a' | % 40
    } 
  >>
  \oneVoice
  f'4 f'4-\mf-\>~ f'8 g' f' es' | %41
  <<
    {
      \voiceOne
      <f' bes'>4-\!-\markup { \left-align \italic "con tenerezza espressiva" }\( c''8 d'' es''2~ | % 42
      es''4 d'' f'' es'' | % 43
      d''2 c''4\) bes'~  | % 44
      bes'8 d'' g'4\(~ g'8 g' e'4 | % 45
    }
    \new Voice
    { \voiceTwo
      <bes d'>2-\p <bes es'>4 f'8 g' | % 42
      <c' a'>4  d'8 es' f'4 c''      | % 43
      <g' bes'>4 a'8 bes' g'2~       | % 44
      g'4 es'4~ es'8 g' e'4          | % 45
    }
  >>
  \oneVoice
  a'4 f' bes'8 d'' f''4~             | % 46
  <<
    { \voiceOne
      f''4 g''^\) f'' es'' | % 47
      d''1                 | % 48
    }
    \new Voice
    { \voiceTwo
      <es' g'>8 g' bes' bes' <g' d''>4 <a' c''>    | % 47
      bes'4 aes'4 g'2-\mf  | % 48
    }
  >>
  \oneVoice
  c''4-\p f'8 g' aes'4 bes'8 c'' | % 49
  <f' des''>4^\( <g' es''>4 
  <<
    { \voiceOne
                     b'2~       | % 50
       b'8^\) d'' c''2 g'8 aes' | % 51
       d''4 bes'~ \tuplet 3/2 4 { bes'8 c'' es'' d'' c'' bes' } | % 52
       a'2\( <bes' d''>2~       | % 53
    }
    \new Voice
    { \voiceTwo
                    d'2~     | % 50
      d'8 f' es'4 \tuplet 3/2 { f'8( es' d' } c'4)     | % 51
      f'2\( es'4. d'8\)      | % 52
      cis'8 <cis' e'> d' e' g'4 f' | % 53
    }
  >>
  \oneVoice
  <bes' d''>4 <a' c''> <g' bes' es''> <g' bes' d''> | % 54
  <es' fis' a' c''>2 <g' bes'>4\) <f' a'~>4 | % 55
  <<
    { \voiceOne
       a'8 c'' f'2 r4    | % 56
       bes'8 d''           % 57 (start)
    }
    \new Voice
    { \voiceTwo
       f'4 s2 f'4~       | % 56
       f'4                 % 57 (start)
    }
  >>
  \oneVoice g'2 e'4      | % 57 (end)
  c''8 es'' c'' es'' a' c'' f' es' | % 58
  << 
    { \voiceOne
      bes'2~ bes'8 d'' f'' r | % 59
      <c'' es''>8 <es'' g''> c''4~ <a' c''>8 <c'' es''> \tuplet 3/2 { <es' a'>8  <a' c''> <bes' d''> } | % 60
    }
    \new Voice
    { \voiceTwo
%     d'4 r r bes'8 es'' | % 59
      d'4 r r \tuplet 5/4 { bes'16( c'' d'' es'' d'') } | % 59
      r4 <es' a'>8 g' r2 | % 60
    }
  >>
  \oneVoice
  <c'' es''>2 r8 <f' a'>8-\mf-\< <f' a' c''> <f' bes' d''> | % 61
  <f' a' c'' dis''>4-\! <e' a' cis'' e''>2.~ |
  <e' a' cis'' e''>2 r8 <e' a'>-\mf-\< <a' cis''> <a' b' d''> | % 63
  <a' cis'' e''>4-\tenuto-\! <a' c'' e'' fis''>2.-\markup { \left-align \italic "cresc." }-\tenuto | % 64
  <b' e'' gis''>2-\p\tenuto <gis' b' e''>4\tenuto <e' gis'>\tenuto | % 65
  <gis' b'>2\tenuto <gis' b' cis''>4\tenuto <fis' b' dis''>\tenuto | % 66
  <<
    { \voiceOne
      <b' e''>1~       | % 67
      <b' e''>2 dis''~ | % 68
      dis''1           | % 69
      d''1~            | % 70
      d''4               % 71 (start)
    }
    \new Voice
    { \voiceTwo
      gis'2 gis'4 r                | % 67
      e'4\( fis'8 gis' <fis' a'>2~ | % 68
      <fis' a'>4 gis' b' a'        | % 69
      gis'2 fis'4\) e'~            | % 70
      \autoBeamOn e'8 gis'           % 71 (start)
    }
  >>
  \oneVoice <e' a'~ cis''~>2.      | % 71 (end)
  <fis' a' cis''>2                   % 72 (start)
  <<
    { \voiceOne
                   b'4-\< b'8 <e' gis'> | % 72 (end)
    }
    \new Voice
    { \voiceTwo
                   e'8 gis' s4      | % 72 (end)
    }
  >>
  \oneVoice
  <e' gis'>2-\! <dis' gis' b'>4 <dis' fis'> | % 73
  <e' gis'>1-\> | % 74
  \clef bass
  e4.-\mf\tenuto fis8 gis4~\tenuto \tuplet 3/2 { gis8 a fis } | % 75
  e8-\< fis gis a-\! cis'4 b | % 76
  r4 <gis b>2-\mf-\tenuto <g b cis'>4-\tenuto | % 77
  <<
    { \voiceOne
      b2 r2                                  | % 78
    }
    \new Voice
    { \voiceTwo
      g4.-\mf a8 b4~ \tuplet 3/2 { b8 c' a } | % 78
    }
  >>
  \oneVoice
  g8-\< a b c'-\! e'4 d'8 \clef treble <bes d' g'>-\p | % 79
  <d' g' bes!>4. <bes d' g'>8-\> <d' g' bes'>4. <g' bes' d''>8 | % 80
  <f' aes' des'' f''>2-\pp <es' g' des'' es''>4 r8 es'-\p | % 81
  <c' es' aes' c''>4. <aes c' es'>8 <c' es' aes'>4.-\> <es' aes' c''>8 | % 82
  <des' ges' bes'>2-\pp           % 83 (start)
  <<
    { \voiceOne
                     s2         | % 83 (end)
      a'!2.          a'4        | % 84
    }
    \new Voice
    { \voiceThree
                     r4 r8 ges' | % 83 (end)
      ges'1                     | % 84
    }
    \new Voice
    { \voiceTwo
                     es'2~      | % 83 (end)
      es'4 es' es' es'8 es'     | % 84
    }
  >>
  \oneVoice
  <f' c''>2 <f' c''>4. f'8      | % 85
  <<
    { \voiceOne
      aes'1                     | % 86
      g'2 <g' bes'>2~\(         | % 87
      <g' bes'>4 <g' bes'>2.~   | % 88
      <g' bes'>2 <bes' des''>~  | % 89
      <bes' des''>4 <bes' des''>4 <des'' f''>2~ | % 90
      <des'' f''>4 <des'' f''>2 <bes' des''>4~  | % 91
      <bes' des''>4 <bes' des''>2 bes'8 g'\)    | % 92
      <g' bes'>4 <g' bes'>2 <g' bes'>4          | % 93
    }
    \new Voice
    { \voiceTwo
      r4 e'2.-\accent-\pp\<\(~ | % 86
      e'4 des'2.~\!            | % 87
      des'2\) f'\accent-\pp\(~ | % 88
      f'4-\pp es'2  <es' g'>4~ | % 89
      <es' g'>2 <es' g'>8 <d'! fis'> <es' f'> <c' fes'> | % 90
      es'2~ es'8 d'! <es' g'>4\)~ | % 91
      <es' g'>4 <es' g'> <des' es'> des' | % 92
      <des' es'>2 <des' es'>8 bes des' c' | % 93
    }
  >>
  \oneVoice
  <es' g'>4 <es' g'>8 g' g' f'4 fes'8 | % 94
  <<
    { \voiceOne
      es'2. es'4~   | % 95
      es'1          | % 96
    }
    \new Voice
    { \voiceTwo
      c'1~          | % 95
      c'4 c'2 c'4   | % 96
    }
  >>
  \oneVoice
  <c' es'>1         | % 97
  <ces' es'>2 <ces' es'> | % 98
  \bar "||"
  \key g \major
  <d' b'>1-\markup { \dynamic p \italic "sempre" }\laissezVibrer | % 99
  R1*3
  b'1-\laissezVibrer       | % 103.1
  R1*3
  fis''1-\laissezVibrer     | % (106.4)
  R1*2
  r4 es''2.-\laissezVibrer  | % 110.2
  R1*2
  r2 c''2-\laissezVibrer  | % 113.3
  R1*3
  g''1-\laissezVibrer       | % (116.3, really 117.1)
  R1
  r2 b'2-\laissezVibrer     | % 119.3
  R1*2
  r4 e''2.-\laissezVibrer   | % 122.2
  R1*2
  cis''1-\laissezVibrer     | % (124.4)
  R1
  dis''1-\laissezVibrer     | % 127.1
  R1
  r4 a''2.-\laissezVibrer   | % 129.2
  R1
  r4 e''2.-\laissezVibrer   | % 131.2
  R1
  r4 b'2.-\laissezVibrer    | % 133.2
  R1
  g''1-\laissezVibrer       | % 135.1
  r2 r4 b'4-\laissezVibrer  | % 136.4
  R1
  r2 c''2-\laissezVibrer    | % 138.3
  R1
  d''1-\laissezVibrer       | % 140.1
  r4 d''2.-\laissezVibrer   | % 141.2
  r2 dis''2-\laissezVibrer  | % 142.3
  r2 r4 b'4-\laissezVibrer  | % 143.4
  R1
  e''1-\laissezVibrer       | % 145.1
  gis'1-\laissezVibrer      | % 146.1
  b'1-\laissezVibrer        | % 147.1
  b'1-\laissezVibrer        | % 148.1
  gis'2.-\laissezVibrer gis'4-\laissezVibrer   | % 149.1 + 149.4
  r2 b'2-\laissezVibrer     | % 150.3
  r4 gis''2.                | % 151.2
  b'2-\laissezVibrer b'2-\laissezVibrer | % 152.1 + 152.3
  b'2.-\laissezVibrer b'4-\laissezVibrer | % 153.1 + 153.4
  R1
  gis'1-\ppp\laissezVibrer | % 155
  gis'1-\ppp\laissezVibrer | % 156
  gis'1-\ppp\laissezVibrer | % 157
  R1*1
  r1-\fermata
}

piano_RHb = {
  \global
  \omit Staff.Clef
  \omit Staff.TimeSignature
  \omit Staff.KeySignature
  \stopStaff
  s1*35
  \undo \omit Staff.Clef
  \undo \omit Staff.KeySignature
  \startStaff
  \clef treble
  \key bes \major
  d'4\tenuto-\p\( a'8 g' fis'2~                     | % 35
  fis'4 g' a' g'                                    | % 36
  f'!2 es'                                          | % 37
  f'4 d' d'4.\) r8                                  | % 38
  \omit Staff.Clef
  \omit Staff.KeySignature
  \stopStaff
  s1*17
  \startStaff
  r2 d'~      | % 56
  d'4 e' r2   | % 57
  \stopStaff
}

piano_LHa = {
  \global
  \set Staff.pedalSustainStyle = #'mixed
  s1\sostenutoOn
  r4
  g,,4_\markup { \martellatoUp \dynamic p } r2  |
  R1*2
  r4 d,4_\martellatoUp r2  |
  R1*3
  r4 des,4_\martellatoUp r2  |
  R1*6
  r4 c,4_\martellatoUp r2  |
  R1*1
  r4 a,,4_\martellatoUp r2  |
  R1*1
  r4 aes,,4_\martellatoUp r2  |
  R1*1
  r4 es,4_\martellatoUp r2  |
  R1*3
  r4 a,,4_\martellatoUp r2  |
  R1*3
% <d fis a d'>2-\f\sostenutoOff\tenuto <e g b e'>4\tenuto <e g c' e'>4\tenuto |
% <fis d'>2 <g c' e'>4\tenuto <g b e'>4\tenuto |
% <fis d'>2 <g c' e'>4\tenuto <g b e'>4\tenuto |
  d'8-\f\sostenutoOff\tenuto( d\tenuto fis\tenuto a\tenuto) b8\tenuto( <e g>)\tenuto c'\tenuto( <e g>\tenuto) | % 29
  <fis, fis>4\tenuto( <d d'>\tenuto) c'8\tenuto( g\tenuto b\tenuto g\tenuto) | % 30
  <<
    { \voiceOne
      s1*3  | % 31-32-33
      b1    | % 34
      bes!1~ | % 35
      bes4 r4 es2 | % 36
      f!4 fis g2~ | % 37
      g4 bes-\> r d' | % 38
      es'4-\pp c' c'4-\< bes8 bes-\! | % 39
      g4 e a f | % 40
      r8 d'8 r4 r4 bes~\( | %41
      bes4 c' g a         | %42
      f4 g d\) r          | %43
      g4 a8 bes c'2\(      | %44
      d'4\) bes bes4.-\pp d'8 | % 45 
      c'4 a <d' f'>4 <bes d'~>4 | %46
      d'4 es' g a           | % 47
      \tuplet 5/4 { g16( a bes c' cis' } d'2) c'8 d'8 | % 48
      es'4\( f' c' d'       | % 49
    }
    \new Voice
    { \voiceThree
      <c' d'>2\tenuto d'2\tenuto\accent~ | % 31
      d'2  c'4\tenuto c'4\tenuto         | % 32
      b2\tenuto a4\tenuto g4\tenuto      | % 33
      fis1(-\markup { \italic "dim." }   | % 34
      f!4)^\pp f4\rest f2\rest           | % 35
      s1*14                              | % 36-37-38-39-40-41-42-43-44-45-47-48-49
    }
    \new Voice
    { \voiceTwo
       a8-\ff\sustainOn( a, a a,) g4\sustainOff\tenuto g\tenuto | % 31
       fis2\tenuto e4\tenuto e\tenuto | % 32
       d1-\markup { \italic "dim." }~ | % 33
       d1 \bar "||" \key bes \major      % 34
       bes,!2-\markup{ \dynamic p \italic "legatissimo, con meno pedale possibile" } bes,2\laissezVibrer  | % 35
       r1                              | % 36
       r1                              | % 37
       c2\tenuto c4\tenuto c\tenuto    | % 38
       f1\tenuto~                      | % 39
       f2. r4                          | % 40
       bes4-\pp a8 g f4 es8 c          | % 41
%      bes4-\pp a8 g f4 \tuplet 5/4 { es16( d des c b, } | % 41
       bes,2 es-\p                    | % 42
       f4 r d es8 f                    | % 43
       g4 f e g                        | % 44
       d2 c                            | % 45
%      f2~ f8 bes es4~                 | % 46
       f2~( \tuplet 3/2 8 { f16 g a bes g f } es4~ | % 46
       es4) c f d                      | % 47
       r2 b4 g4                        | % 48
       r2 f2                           | % 49
    }
  >>
  \oneVoice
  <des aes>2\) r2 | % 50
  c4 d8 es f2   | % 51
  aes2 <es g>4 f8 g | % 52
  a2\( f4 f        | % 53
  <<
    { \voiceOne
      bes2~ bes4 g | %54
      c'2\) d'    | % 55
      r2 c'~      | % 56
      c'2 bes     | % 57
      c'2 r2      | % 58
    }
    \new Voice
    { \voiceTwo
      fis4 d g es  | % 54
      a4( fis) bes b | % 55
      c'4 a a a8( f) | % 56
      c1             | % 57
      f1             | % 58
    }
  >>
  \oneVoice
  f1                 | % 59
  <<
    { \voiceOne
      <c' es'>8 es'8 <a c'>4~ <a c'>8 <c' es'> r es' | % 60
      <es' f'>4 r
    }
    \new Voice
    { \voiceTwo
      f1~ | % 60
      f8 g a4
    }
  >>
  \oneVoice r8 <c c'>8-\mf-\< <a, a> <g, g> | % 61
  <f c'>4-\!
  <<
          { \voiceOne cis'2.~ | cis'4 s4 }
    \new Voice { \voiceTwo e4 \tuplet 3/2 4 { e4-\f fis8 gis a b } | bis4 cis' }    
  >>
  \oneVoice r8 <e a>8-\mf-\< <a cis'> <a b d'> | % 63
  r8-\! <e, e>8-\f-\< <a, a> <b, b>-\! <d d'>4-\tenuto <c c'>-\tenuto | % 64
% <b, b>2-\tenuto <b, b>4-\tenuto <b, a>-\tenuto | % 65
  b,8( b b, b) b,8( b) b,( a) | % 65
% <b, b>2-\tenuto <b, b>4-\tenuto <b, a>-\tenuto | % 66
  b,8( b b, b) b,( b) b,( a)  | % 66
  <<
    { \voiceOne
      gis2 r2 | % 67
      b2 dis'2~ | % 68
      dis'1     | % 69
      r8 b-\< cis' d'-\! r2 | % 70
      r4 <a cis'>2 a4       | % 71
      dis'4 b <gis b~>2     | % 72
      b4 cis' b  a          | % 73
    }
    \new Voice
    { \voiceTwo
      e1-\markup { \left-align \italic "dim." }~ | % 67
      e2 r8 fis8-\markup { \right-align \italic "con espressione" }-\< gis a-\!| % 68
      cis'4 b r2 | % 69
      e1         | % 70
      e1~        | % 71
      e1         | % 72
      e1         | % 73
    }
  >>
  \oneVoice
  <e gis b>1-\>  | % 74
  r8-\! e,4-\mf\tenuto fis,8 gis,4~\tenuto \tuplet 3/2 { gis,8 a, fis, } | % 75
  e,8-\< fis, gis, a,-\! cis4\tenuto b,\tenuto | % 76
  r4 <e, e>2\tenuto <e, e>4-\tenuto | % 77
  r8 d( d,4-\tenuto) <d, d>4-\tenuto r | % 78
  R1*1
  r4 d2 <bes, d g>4 | % 80
  <<
    { \voiceOne
      <aes des'>2 <g des'>4 r | % 81
    }
    \new Voice
    { \voiceTwo
      es2. es4                | % 81
    }
  >>
  \oneVoice
  es2 es4 es | % 82
  fes2 es | % 83
  <<
    { \voiceOne
      c'2.  c'8 r | % 84
    }
    \new Voice
    { \voiceTwo
      r2 r4 r8 <ges a c'>8 | % 84
    }
  >>
  \oneVoice
  <f bes des'>4. <des f bes>8 <f bes des'>4.-\> <bes des'>8 | % 85
  <bes des'>1-\! | % 86
  <<
    { \voiceOne
      des'4 r4 r2 | % 87
    }
    \new Voice
    { \voiceTwo
      <es! bes>1   | % 87
    }
  >>
  \oneVoice
  es2. es4        | % 88
  <<
    { \voiceOne
      des'2-\pp~ des'4. c'8 | % 89
      des'2 bes4 g~         | % 90
      g2 bes4 des'8 c'      | % 91
      des'8 <c' es'> <bes des'>4 <g bes>2~ | % 92
      <g bes>4 r4 g4 r | % 93
      g4 g8 bes <bes d'>4 <g des'>8 g | % 94
      bes4 aes8 g-\> aes2~  | % 95
      aes4-\! r r aes       | % 96
      aes4 g2  f4           | % 97
      aes2 aes-\>           | % 98
    }
    \new Voice
    { \voiceTwo
      es4. d!8 es2~         | % 89
      es4. d!8 es2~         | % 90
      es4. d!8 es2~         | % 91
      es4. d!8 es2~         | % 92
      es4 <es g>8 <f aes> es2~ | % 93
      es8 d d2 d!4          | % 94
      es1~                  | % 95
      es2.  es4             | % 96
      es1                   | % 97
      es1                   | % 98
    }
  >>
  \oneVoice
  \bar "||"
  \key g \major
  r2-\! d,2-\p\laissezVibrer | % 99
  R1*3
  r2 d,2-\laissezVibrer     | % 103.1
  R1*3
  r2 cis,2-\laissezVibrer   | % (106.4)
  R1*2
  r2 r4 es,4-\laissezVibrer  | % 110.2
  R1*3
  des,1-\laissezVibrer       | % 113.3
  R1*2
  r2 d,2-\laissezVibrer      | % (116.3, really 117.1)
  R1*2
  b,,1-\laissezVibrer       | % 119.3
  R1*1
  r2 r4 e,!4-\laissezVibrer | % 122.2
  R1*2
  r2 e,2-\laissezVibrer     | % (124.4, really 125.1))
  R1
  r2 e,2-\laissezVibrer       | % 127.1
  R1
  r2 r4 e,4-\laissezVibrer   | % 129.2
  R1
  r2 r4 e,4-\laissezVibrer   | % 131.2
  R1
  r2 r4 d,4-\laissezVibrer   | % 133.2
  R1
  r2 d,2-\laissezVibrer     | % 135.1
  R1
  r4 b,,2.-\laissezVibrer   | % 136.4
  R1
  a,1-\laissezVibrer        | % 138.3
  r2 aes,2-\laissezVibrer       | % 140.1
  r2 r4 g,4-\laissezVibrer   | % 141.2
  R1
  b,,1-\laissezVibrer         | % 142.3
  r4 b,,2.-\laissezVibrer      | % 143.4
  r2 e,2-\laissezVibrer       | % 145.1
  r2 e,2-\laissezVibrer      | % 146.1
  r2 d,2-\laissezVibrer      | % 147.1
  r2 e,2-\laissezVibrer      | % 148.1
  r2 e,2-\laissezVibrer      | % 149.1
  r4 dis,2.-\laissezVibrer   | % 149.4
  b,,2.-\laissezVibrer b,,4-\laissezVibrer   | % 150.3 + 151.2
  r2 e,2-\laissezVibrer      | % 152.1
  e,2-\laissezVibrer e,2-\laissezVibrer  | % 152.3 + 153.1
  r4 e,2.-\laissezVibrer | % 153.4
  <e,, e,>1-\ppp\laissezVibrer | % 155
  r2 <dis,, dis,>-\ppp\laissezVibrer | % 156
  <e,, e,>1-\ppp\laissezVibrer | % 157
  R1*1
  r1-\fermata
}

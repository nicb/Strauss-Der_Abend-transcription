bs_clar = \relative c {
  \global
  s1 \bar ""
  \compressFullBarRests { R1*8 }
  r8 es8_\mf(^\markup \right-align { \italic "con espressione" } es2 es4
  fes1)~fes4 es4 \tuplet 3/2 { r es4 es }
  \tuplet 3/2 { g4 aes bes } es,2~es1~es4 r4 r2
  \compressFullBarRests { R1*1 }
  r8 aes8_\f aes2 aes4
  es'1~es4 f,4 r2
  %
  % bar 19
  %
  bes1_\pp |
  b!2 b |
  \tuplet 3/2 { aes2_\mf g4 } \tuplet 3/2 { f4 es d } |
  es4 c r2 |
  \tuplet 3/2 { g'2_\mf f4 } \tuplet 3/2 { es4 d c } |
  bes4 a r2 |
  d1_\pp~ |
  d2 d |
  d'1~ |
  d4. cis8 b4 a8 g |
  fis2_\f e4 e |
  fis2 g4 g |
  a2_\ff g4 g |
  fis2 e4 e |
  d1_\markup { \italic "dim." }~ |
  d1 \bar "||"
  \key bes \major
  bes2_\p bes4 r4 |
  %
  \compressFullBarRests { R1*2 }
  %
  % bar 38
  %
  c2_\pp c4 c |
  f1~ |
  f1 |
  bes4_\pp a8 g f4 d8 c |
  bes2 es_\p |
  f4 r d es8 f |
  g4 f \tuplet 3/2 { e8( f a } \tuplet 3/2 { g8 f) e } |
  d2 c |
  f2~ f8 bes es,4~( |
  \tuplet 3/2 { es8 d es } \tuplet 3/2 { c8 d es } \tuplet 3/2 { f8 es f } \tuplet 3/2 { d8 es f } |
  g8 bes d2) g,4 |
  r2 f2 |
  aes2_\p\> d4_\! r |
  c,4 d8 es f2( |
  aes2) g4 r |
  r2 r4 f_\p |
  fis2( \tuplet 3/2 { g8 f g) } \tuplet 3/2 { es8( f g) } |
  a2( bes4) b_\> |
  c2._\pp a8( f) |
  c1 |
  f1 |
  f1~ |
  f4 r4 r2 |
  r2 r8 f8_\mf\< c' bes |
  a4_\! a4 \tuplet 3/2 { e4_\ff fis8 } \tuplet 3/2 { gis8 a b } |
  bis4 cis4 r2 |
  %
  % bar 64
  %
  r8 e,_\f\< a b_\! d4 c |
  r4 b2_\p e,4 |
  b2 b4 b |
  e1_\markup { \center-align { \italic "dim." } } | % 67
  e1_\pp~ |
  e1  |
  e1  | % 70
  e1~ |
  e1  |
  e1  | % 73
  e1  |
  e4._\mf fis8 gis4~ \tuplet 3/2 { gis8 a fis } |
  e8_\< fis gis a_\! cis4 b |
  r4 e,2 e4 |
  d2 d4 r4 |
  %
  \compressFullBarRests { R1*2 }
  %
  % bar 81
  %
  es2._\pp es4 |
  es2 es4 es4 |
  fes2 es2 |
  %
  \compressFullBarRests { R1*3 }
  %
  % bar 87
  %
  es1_\pp |
  es2. es4 |
  es4.( d8 es2~ |
  es4. d8 es2~ |
  es4. d8 es2~ |
  es4. d8 es2~ |
  es4. d8 es2~ |
  es4) d2 d4   |
  es1~ |
  es1_\>~ |
  es4_\! r4 r2 |
  %
  \compressFullBarRests { R1*1 }
  \bar "||"
  \key g \major
  %
  % bar 99
  %
  r2 d,4(_\p e4 |
  fis2 g4 a  |
  b4 a b c |
  d2 cis4 d~ |
  d4 e fis g8 a8 |
  b1)_\> |
  r4_\! b,4_\p b2~ |
  b2 b |
  cis1 |
  cis1 |
  %
  % bar 109
  %
  bes4(_\markup { \dynamic p \italic "con espressione" } c!) des es8( f) |
  aes2( g4) f |
  es1 |
  es2 es |
  e!2( f  |
  des1   |
  es1)   |
  es1    |
  \compressFullBarRests { R1*4 }
  %
  % bar 121
  %
  e,!1_\p |
  e1     |
  e1~    |
  e1~    |
  e1\breathe |
  e1~    |
  e1~    |
  e1~    |
  e1     |
  e1     |
  e1(    |
  a2) c! |
  \compressFullBarRests { R1*2 }
  %
  % bar 135
  %
  d1_\pp |
  d2 d   |
  b1(    |
  bes1)  |
  a1     |
  aes1   |
  g1~    |
  g1_\<  |
  b_\mf~ |
  b_\markup { \center-align { \italic "dim." } } |
  e,1_\pp |
  e2_\p e4 e'4 |
  d4.^\accent d8 d2 |
  gis4_\markup { \dynamic p \italic "tranquillo assai" } gis8 fis e4 gis,8 a |
  b2 r4 e_\pp |
  dis4. dis8 dis2 |
  e1     |
  r2 b'4._\p( a8) |
  gis4 cis,8(_\< dis) e2_\pp~ |
  e2. b4 |
  e,1_\ppp~ |
  e2 dis |
  e1~ |
  e1~ |
  e1^\fermata \bar "|."
}

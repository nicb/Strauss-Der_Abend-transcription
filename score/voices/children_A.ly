children_A = \relative c''' {
  \global
  \autoBeamOff
  % note held for 19 bars
  %                  10               19
  s1 \bar ""
  g1^\markup { \dynamic pp \italic "(senza respiro udibile)" }~g~g~g~g~g~g~g~g~g~g~g~g~g
  g~g~g~g~g~g2.( f4 es1) es2 es d1(~d2 cis)
  %
  % bar 25, letter 2
  %
  a'1^\pp~a~a~a^\markup { \italic "cresc." }~ |
  a2^\f c,4 e |
  fis2 e4 e
  d1^\markup { \italic "dim." }~d~d~d~ \bar "||" \key bes \major d4^\ppp r4 r2
  \compressFullBarRests { R1*27 }
  %
  % bar 63
  %
  r2 r8 e,^\< a b |
  cis4^\! e2.^\markup { \center-align { \italic "cresc." } } |
  %
  % bar 65
  %
  gis2^\p e4 gis,4 b2 cis4 dis4
  e1^\p^\markup { \italic "dim." }~e2 dis^\pp~dis1
  d1~d4 cis2.~cis2 b4.^\< gis8^\! gis2 gis4^\> fis4 e1^\pp~e~e~e4 r4 r2
  %
  % bars 78-98 (20 bars)
  %
  \compressFullBarRests { R1*21 }
  \bar "||" \key g \major
  %
  % bar 99
  %
  b'1^\pp~b~b~b~b~b2 b2
  %
  % bar 105
  %
  r4 b4^\p\( b2~ | % 105
  b4 b4^\< cis4 dis4^\! | % 106
  fis1 e1\)^\> es1^\p~es bes2 r2
  %
  \compressFullBarRests { R1*5 }
  %
  % bar 117
  %
  g'1^\pp b,2. b4 b1 cis2 dis2 e1~e~e2 r2
  %
  \compressFullBarRests { R1*4 }
  %
  % bar 128
  %
  \autoBeamOn
  b4(^\markup { \dynamic "p" \italic "con espressione" } cis4) dis4 e8( fis8)
  a2( gis2 fis2) e2~e1^\markup { \italic "dim." }~e4 r4 r2
  %
  % bar 133
  %
  b1^\pp g1 g'1 b,2 b2 b1( c1 cis1 d1~d1^\markup { \italic "cresc." } dis1~dis1)^\sfz^\markup { \italic "dim." } dis2 dis2 e1^\pp
  %
  % bar 146
  %
  gis,2^\p gis2
  %
  \compressFullBarRests { R1*2 }
  %
  % bar 149
  %
  r2 r4 gis4^\pp |
  b4. b8 b2 |
  b1 |
  %
  % bar 152
  %
  r2 b2^\pp~ | b1 | b1 | gis1^\ppp~ | gis1~ | gis1~ | gis1~ | gis1^\fermata \bar "|."
}

children_A_text = \lyricmode {
  Strah -- Strah -- len der Gott!
  Strah -- len den Wa -- gen hi -- nab!
  Er -- kennt dein Herz sie?
  The -- tys, die gött -- li -- che winkt.
  The -- tys, The -- tys, die gött -- li -- che winkt.
  Ru -- het!
  Ihr folgt die sü -- sse Lie -- be.
  Lie -- bet.
  Phö -- bus, der lie -- ben -- de ruht.
  Ru -- het und -- lie -- bet!
  Phö -- bus, Phö -- bus, der lie -- ben -- de ruht.
  Phö -- bus,
  der lie -- ben -- de ruht.
  Phö -- bus ruht.
}

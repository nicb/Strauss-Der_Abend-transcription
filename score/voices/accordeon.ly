accordeon_l = \relative c''' {
  \global
  s1 \bar ""
  R1*3
  r2 g2_\p~ |
  g4. fis8 e4 b8 d8 |
  g1~ | g1~ |
  g2 f4 f4 |
  es1 | r1
  g2_\p~g4. f8 |
  es4 bes8 des8 g2~ |
  g2. r4 |
  R1*3
  %
  % bar 17
  %
  g2_\p~ g4. f8 |
  es4 a,8 c g'2~ |
  g2. f4 |
  es2. d4 |
  c1 |
  c2 c |
  bes1(~ |
  bes2 a) |
  r1 |               % 25
  a'2_\pp~ a4. g8 |
  fis4 d8 a d2~ |
  d1_\markup { \center-align { \italic "cresc." } } |
  <fis, a>2_\f <e g>4 <e g> |
  <d fis>2 <c e>4 <b e>4 | % 30
% \tuplet 5/4 4 { c16_(-\markup { \dynamic ppp \italic ", molto legato" }-\< d c d c  d c d c d   c d c d c  d c d c d } | % 31
  \repeat tremolo 16 { c32-\markup { \dynamic ppp \italic "sempre molto legato" }-\<_\(_( d } | % 31 
% \tuplet 5/4 4 { c16 d c d c  d c d c d   c d c d c  d c d c d } | % 32
  \repeat tremolo 16 { c32 d } | % 32
% \tuplet 5/4 4 { c16 d c d c   d c d c d-\! } 
  \repeat tremolo 8 { c32 d-\! } 
  <<
%     { \voiceOne \stemDown \slurDown \tieDown \tupletDown \tuplet 5/4 4 { c_\markup { \dynamic f \italic "dim." } d c d c  d c d c d } |
%                 \tuplet 5/4 { c16 d c d c } d2.~ | 
      { \voiceOne \stemDown \slurDown \tieDown \tupletDown \repeat tremolo 8 { c32_\markup { \dynamic f \italic "dim." } d } |
                  \repeat tremolo 4 { c32 d_)~ } d2.~ |
      }
    \new Voice { \voiceTwo \stemUp \slurUp \tieUp \tupletDown d'2~ | d2 d4 d4\rest | }
  >>
  \bar "||" \key bes \major 
  << { \voiceOne \stemDown d,4_\)_\ppp }
     \new Voice { \voiceTwo \stemUp \slurUp bes'4^\p }
  >>
% << { \voiceOne d1_\markup { \center-align { \italic "dim." } }~ | d1~ | d1~ | d1~ \bar "||" \key bes \major \stemDown d4^\ppp }
%    \new Voice { \voiceTwo a1~ | a1 | s1 | s1 | s4 }
%    \new Voice { \voiceThree s1 | s1 | r2 d'2^\markup { \dynamic f \italic "dim." }~ | d2 d4 r4 | bes4_\p }
% >>
  \oneVoice \stemNeutral
      a8 g8 fis2~ |
  fis4 g b c |
  f,2 es |
  es4( d c) bes |
  c2( bes4. d8 |
  d2)_\mf\> c4_\! r4 |
  r4 d4 d8 c8 bes4(~ |
  \tuplet 3/2 { bes8 c d } \tuplet 3/2 { c8 bes a) } r2 |
  c4 d8 es f4 c' |
  d,4. d8 g2~ |
  g4 bes, r2 |
  f'4. f8 \tuplet 3/2 { f8( es f } \tuplet 3/2 { d8 es f } |
  g4. bes8 d4) c |
  d,2_\mf\> g4_\! r4 |
  es4_\p f8 g \tuplet 3/2 { aes8( bes c } \tuplet 3/2 { bes8 aes g } |
  \tuplet 3/2 { f8 g aes) } \tuplet 3/2 { g8( f es) } d2~ |
  d8 f es4(~ \tuplet 3/2 4 { es8 d es c8 d es } | % 51
  \tuplet 3/2 4 { f8 es f d8 es f } g2~ | % 52
  \tuplet 3/2 4 { g8 bes a) g8 f e } d2(~ | % 53
  d4 es!2 es8) f | % 54
  fis2 g4 a | % 55
  f4(~ \tuplet 3/2 { f8 e d) } <a c>4 f'_\markup { \left-align { \dynamic p \italic "con espressione" } }~ | % 56
  f4 e d c8 bes | % 57
  a4 r4 f'2_\p~ | % 58
  f2~ f4. g8 | % 59
% << { \voiceOne s2. | s1 | a4(^\markup { \dynamic p \italic "con espressione" } b8 cis d2~ | d4 c! es d | c2 bes4) a~ | a8 c s2. |
%      bes8^\p d g,4(~ \tuplet 3/2 { g8 f g } \tuplet 3/2 { e8 f g } | a4) c8 es a,4 a~ | a4 g \stemDown d4._\p f8 | }
% \new Voice { \voiceTwo \stemNeutral
% >>
  %
  % bar 60
  %
% \oneVoice
  \tuplet 3/2 4 { <c, a'>8( <bes g'> <c a'>    <a f'>8 <c a'> <d bes'>)   <es c'>8( <d bes'> <es c'>)   <f a>8( <a c> <bes d>) } |
  <c es>2 r2 |
  R1*1
  r2 r8 <a, e'>_\mf_\< <cis cis'> <d b'> |
  r8_\! e_\f_\< a b_\! d4 c |
  b2_\p gis4 e |
  b'2 <b cis>4 <b dis> | % 66
% << { \voiceOne e1^\markup { \left-align { \italic "dim." } }~ | e2 dis^\pp~ | dis1 | d!1~ | d4 }
%    \new Voice { \voiceTwo b1_\markup { \left-align { \italic "dim." } }~ | b1~ | b1_\pp~ | b1~ | b4 }
% >>
% \oneVoice <a cis>2.~ |
% <a cis>2 <gis b>4._\< <e gis>8 |
% \tuplet 5/4 4 { e16(-\pp b e b e   b e b e b   e b e b e   b e b e b } | % 67
  \repeat tremolo 16 { e32-\pp(\( b } | % 67
% \tuplet 5/4 4 { e16 b e b e   b e b e b    dis b dis b dis   b dis b dis b } | % 68
  \repeat tremolo 8 { e32 b) } \repeat tremolo 8 { dis32( b } | % 68
% \tuplet 5/4 4 { dis16 b dis b dis   b dis b dis b   dis b dis b dis   b dis b dis b } | % 69
  \repeat tremolo 16 { dis32 b) } | % 69
% \tuplet 5/4 4 { d!16 b d b d   b d b d b   d b d b d   b d b d b } | % 70
  \repeat tremolo 16 { d!32( b } | % 70
% \tuplet 5/4 4 { d16 b d b d)   a( cis a cis a   cis a cis a cis   a cis a cis a } | % 71
  \repeat tremolo 4 { d32 b)\) } \repeat tremolo 12 { a( cis } | % 71
% \tuplet 5/4 4 { cis16 a cis a cis   a cis a cis a) } <gis b>4._\< <e gis>8 |
  \repeat tremolo 8 { a32 cis) } <gis b>4. <e gis>8 | % 72
  <cis gis'>2 <dis gis>4_\> <dis fis> |
  e1_\pp~ |
  e4 r r2 |
  R1*1
% r4 b2~ \tuplet 5/4 { b16( cis b cis b) } | % 77
  r4 b2~ \repeat tremolo 4 { b32( cis) } | % 77
% \tuplet 5/4 4 { d16( b d b d  b d b d b)  d( b d b d) } r4 | % 78
  \repeat tremolo 12 { d32( b) } r4 | % 78
  r2 r4 r8 <bes! d>8_\p |
  <d g>4. <bes d>8-\> <d g>4. <g bes! d>8 |
  << % { \voiceOne <bes des>2.._\pp }
%    { \voiceOne \tuplet 5/4 4 { bes16(-\pp des bes des bes   des bes des bes des   bes des bes des bes } des16 bes) }
   { \voiceOne \repeat tremolo 14 { bes32-\pp( des) } r8 }
     \new Voice { \voiceTwo aes2 g4. es8_\p | }
  >>
  \oneVoice
  <c es aes>4. <aes c es>8 <c es aes>4._\> <es aes c>8 | % 82
% <des ges bes>1_\pp | % 83
% <c es a>2. <c es a>4 | % 84
% <des f c'>2 <des bes'>2 | % 85
% \tuplet 5/4 4 { ges16(-\pp bes ges bes ges   bes ges bes ges bes   ges bes ges bes ges   bes ges bes ges bes) } | % 83
  \repeat tremolo 16 { ges32-\pp( bes) } | % 83
% \tuplet 5/4 4 { es,16( a es a es   a es a es a   es a es a es)   a( es a es a) } | % 84
  \repeat tremolo 12 { es,32( a) } \repeat tremolo 4 { es32( a) } | % 84
% \tuplet 5/4 4 { f16( c' f, c' f,   c' f, c' f, c')   des,( bes' des, bes' des,   bes' des, bes' des, bes') } | % 85
  \repeat tremolo 8 { f32( c') } \repeat tremolo 8 { des,32( bes' } | % 85
% \tuplet 5/4 4 { des,16( aes' des, aes' des,  aes' des, aes' des, aes'   des, aes' des, aes' des,  aes' des, aes' des, aes' } | % 86
  \repeat tremolo 16 { des,32 aes' } | % 86
  des,4) r \shape #'((0 . -0.3) (0 . -1.0) (0 . -1.5) (0 . -0.3)) Slur <g bes>2(^\pp~ |
  \tuplet 3/2 { <g bes>8 <aes c> <fes aes> } <g bes>2.~ |
  <g bes>2~ \tuplet 3/2 { <g bes>8 <fis a> <f aes> } % split voices
  << { \voiceOne g4~ \shape #'((0 . -3.0) (0 . -3.0) (0 . -2.0) (0 .  -2.0)) Slur |
        g2.^\< \tuplet 3/2 { bes8^\! a aes } | }
        \new Voice { \voiceTwo es4~ | es2 g8 fis f fes | }
  >>
  \oneVoice
  <es g>2)^\> <bes des>4^\! <des g>^\pp | <g bes>2 <des g>4 <bes des>~ | <bes des>4 r4 r2 | r1 |
  R1*1
% r2 r4 <es, c' es>4_\pp | % 96
% <es c' es>1 | % 97
  \clef bass
% r2 r4 \tuplet 5/4 { es16(-\ppp c es c es) } | % 96
  r2 r4 \repeat tremolo 4 { es32(-\ppp-\< c) }    | % 96
% \tuplet 5/4 4 { c16( es c es c   es c es c es   c es c es c   es c es c es) } | % 97
  \repeat tremolo 8 { c32( es-\!) } \repeat tremolo 8 { c32( es) } | % 97
% <es ces' es>2 <es ces' es>
% \tuplet 5/4 4 { ces( es ces es ces   es ces es ces es)  ces( es ces es ces  es ces es ces es) } % 98
  \repeat tremolo 16 { ces32( es) } | % 98
  \bar "||"
  \key g \major
% \tuplet 5/4 4 { d16(-\ppp b d b d   b d b d b   d b d b d   b d b d b } | %  99
  \repeat tremolo 16 { d32-\ppp(-\< b } | % 99
% \tuplet 5/4 4 { d16 b d b d   b d b d b   d b d b d   b d b d b } |   % 100
  \repeat tremolo 16 { d32-\pp b } | % 100
% \tuplet 5/4 4 { d16 b d b d   b d b d b   d b d b d   b d b d b } |   % 101
  \repeat tremolo 16 { d32 b } | % 101
% \tuplet 5/4 4 { d16 b d b d   b d b d b   d b d b d   b d b d b } |   % 102
  \repeat tremolo 16 { d32 b } | % 102
% \tuplet 5/4 4 { d16 b d b d   b d b d b   d b d b d   b d b d b } |   % 103
  \repeat tremolo 16 { d32 b) } | % 103
% \tuplet 5/4 4 { dis16 b dis b dis   b dis b dis b   dis a dis a dis   a dis a dis a) } | % 104
  \repeat tremolo 8 { dis32( b) } \repeat tremolo 8 { dis32( a) } | % 104
% << { \voiceOne d'1(~ | d1~ | d1~ | d1~ | d1 | dis1) | }
%   \new Voice { \voiceTwo b1(~ | b1~ | b1~ | b1~ | b1~ | b2 a2) }
% >>
% \oneVoice
% r4 <a b dis>4_\p <a b dis>2 | % 105
  \clef treble 
% <a b dis>2 <a b dis> |
% r4 \tuplet 5/4 4 { a16(-\pp-\< dis a dis a)   dis(-\p a dis a dis  a dis a dis a) } | % 105
  r4 \repeat tremolo 4 { a32-\pp-\<( dis) } \repeat tremolo 8 { dis32-\p( a) } | % 105
% \tuplet 5/4 4 { dis16( a dis a dis   a dis a dis a)    dis( a dis a dis   a dis a dis a) } | % 106
  \repeat tremolo 8 { dis32( a) } \repeat tremolo 8 { dis32( a) } | % 106
% <gis cis fis>1 |
% \tuplet 5/4 4 { gis16( fis' gis, fis' gis,    fis' gis, fis' gis, fis'    gis, fis' gis, fis' gis,   fis' gis, fis' gis, fis') } | % 107
  \repeat tremolo 16 { gis32( fis') } | % 107
% <gis cis e>1 |
% \tuplet 5/4 4 { gis,16( e' gis, e' gis,    e' gis, e' gis, e'    gis, e' gis, e' gis,   e' gis, e' gis, e') } | % 108
  \repeat tremolo 16 { gis,32( e') } | % 108
% << { \voiceOne es1-\> | es2^\! c2\rest | f2^\markup { \dynamic mf \italic "con espressione" } \stemDown \tieDown b,4\rest c | a!2 bes2^\> | s1^\! | }
%   \new Voice { \voiceTwo g!2_( bes | bes1-\> | bes2_\!_) s2 | s1 | }
%   \new Voice { \voiceThree cis1 | cis1 | cis1(~ | cis1~ | cis2 c) | }
% >>
% \oneVoice
% \tuplet 5/4 4 { g,16( cis es g, cis   es g, cis es g,   cis es bes cis es   bes-\> cis es bes cis } | % 109
% \repeat tremolo 8 { <g, cis>32-\>( es') } \repeat tremolo 8 { <bes cis>32-\!( es) } | % 109
% \tuplet 5/4 4 { es16-\! bes cis es bes   cis es bes cis es   bes cis es bes cis  es  bes cis es bes) } | % 110
% \repeat tremolo 16 { <bes cis>32( es) } | % 110
% \tuplet 5/4 4 { cis16-\mp( f bes, cis f   bes, cis f bes, cis)   bes( cis bes cis bes)  bes( c cis bes c) } | % 111
% \repeat tremolo 8 { <bes cis>32-\markup { \dynamic mf \italic "con espressione" } f' } \repeat tremolo 4 { bes,32( cis) } \repeat tremolo 4 { bes32( <c cis>) } | % 112
  <<
     { \voiceOne
        \repeat tremolo 16 { cis32-\>( es) } | % 109
        es2-\! a\rest                        | % 110
        cis,1                                | % 111
     }
     \new Voice
     { \voiceTwo
        g2\( bes~                            | % 109
        \repeat tremolo 16 { bes32-\!( cis) } | % 110
        \repeat tremolo 8 { bes32-\markup { \dynamic mf \italic "con espressione" }(\) f') } r4 c! | % 111
     } 
  >>
  \oneVoice
% \tuplet 5/4 4 { a16( cis a cis a   cis a cis a cis)  bes( cis bes cis bes   cis bes cis bes cis~) } | % 112
  \repeat tremolo 8 { a32( cis) } \repeat tremolo 8 { bes32( cis) } | % 112
  bes'2-\markup { \dynamic p \italic "con espressione" }( aes) | % 113
  <g c>2 <f bes> | % 114
  <es aes>1  | % 115
  des2 r | % 116
% <g b d g>1_\pp |
% \tuplet 5/4 4 { b16-\ppp-\<( g' b, g' b,   g' b, g' b, g'   b, g' b, g' b,   g' b, g' b, g') } | % 117
  \repeat tremolo 16 { b32-\ppp-\<( g') } | % 117
% <g b d g>2. <g b d g>4 |
% \tuplet 5/4 4 { b,16( g' b, g' b,  g' b, g' b, g'   b, g' b, g' b,)   g'( b, g' b, g'-\!) } | % 118
  \repeat tremolo 12 { b,32-\!( g') } \repeat tremolo 4 { b,32( g') } | % 118
% <b dis gis>1 |
% \tuplet 5/4 4 { b,16( gis' b, gis' b,   gis' b, gis' b, gis'   b, gis' b, gis' b,   gis' b, gis' b, gis') } | % 119
  \repeat tremolo 16 { b,32( gis') } | % 119
% <b dis gis>2 <a b dis fis> |
% \tuplet 5/4 4 { b,16( gis' b, gis' b,  gis' b, gis' b, gis')   b,( fis' b, fis' b,   fis' b, fis' b, fis') } | % 120
  \repeat tremolo 8 { b,32( gis') } \repeat tremolo 8 { b,32( fis') } | % 120
% << { \voiceOne e'1~ | e1~ | e2 }
%   \new Voice { \voiceTwo b1~ | b2 c2\rest | s2 }
%   \new Voice { \voiceFour gis2. b4\rest | s1 | s2 }
% >>
% \oneVoice
% \tuplet 5/4 4 { e'16-\ppp(-\< gis, b e gis,  b e gis, b e   gis, b e gis, b-\pp  e b e b e } | % 121
% \repeat tremolo 12 { <gis b>32-\ppp-\<(^\( e) } \repeat tremolo 4 { b32-\pp( e) } | % 121
  <<
    { \voiceOne
      \repeat tremolo 16 { b,32-\ppp-\<-\shape #'((0.0 . 2.2) (0.0 . 2.2) (0.0 . 2.2) (0.0 . 2.2))(^\shape #'((0.0 . 1.6) (0.0 . 1.6) (0.0 . 1.6) (0.0 . 1.2))\( e) } | % 122
    }
    \new Voice
    { \voiceTwo
      gis,2. g4\rest                 | % 122
    }
  >>
  \oneVoice
% \tuplet 5/4 4 { b16 e b e b   e b e b e~ } e2~ | % 122
  \repeat tremolo 8 { b32(-\! e)~ } e2~ | % 122
  e2\) r | % 123
  dis4(_\markup { \dynamic p \italic "con espressione" } e) fis gis8( a) |
  <<
    { \voiceOne cis2 b( | a2) gis4 a8( b) | dis2( cis) | dis,4( e) fis gis8( a) | cis2( b~ | b1 | a2. e4 | cis2) c_\markup { \italic "dim." } | }
    \new Voice { \voiceTwo b4( cis) dis e8( fis) | e4( fis) r fis( | e1) | b4( cis dis2~ | dis2) e(~ | <cis e>4 d) e2 | s1*2 }
  >>
  \oneVoice
  d1_\pp |
  b1     | % 134
% << { \voiceOne b'1( | c1 | cis1 | d1~ | d1 | dis1~ | dis)^\sfz | dis2 dis | e1-\pp }
%  \new Voice { \voiceTwo gis,1(~ | gis1 | a1 | bes1~ | bes2) b2~ | b2 c2-\sf~ | c2 cis4-\markup { \italic "dim" } b | a2 gis4 fis4 | e1 }
% >>
% <d g b d>1_\pp~ | % 135
% \tuplet 5/4 4 { d'16-\ppp-\<( b g d' b   g d' b g d'   b-\pp g d' b g   d' b g d' b } | % 135
% \repeat tremolo 16 { d'32-\ppp-\<_( <b g> } | % 135
% <d g b d>2 <d, g b d>2 | % 136
% \tuplet 5/4 4 { g16 d' b g d'   b g d' b g)   d( g, d' g, d'   g, d' g, d' g,) } | % 136
% \repeat tremolo 8 { d32-\pp <b g>) } \repeat tremolo 8 { g32(  d) } | % 136
  <<
    { \voiceOne
      \repeat tremolo 16 { d'32-\ppp-\<( b } | % 135
      \repeat tremolo 8 { d32-\pp b) } \repeat tremolo 8 { d,32( g,) } | % 136
    }
    \new Voice
    { \voiceTwo
      g'1~ | % 135
      g2 e,2\rest | % 136
    }
  >>
  \oneVoice
% \tuplet 5/4 4 { b'16-\ppp-\<( gis b gis b   gis b gis b gis   b gis b gis b gis b gis b gis } | % 137
  \repeat tremolo 16 { b''32-\ppp-\<^(^\( gis^) } | % 137 
% \tuplet 5/4 4 { c16-\pp gis c gis c   gis c gis c gis   c gis c gis c   gis c gis c gis } | % 138
  \repeat tremolo 16 { c32-\pp^( gis^) } | % 138
% \tuplet 5/4 4 { cis16 a cis a cis  a cis a cis a   cis a cis a cis  a cis a cis a }   | % 139
  \repeat tremolo 16 { cis32^( a^) } | % 139
% \tuplet 5/4 4 { d16 bes d bes d   bes d bes d bes   d bes d bes d   bes d bes d bes }  | % 140
  \repeat tremolo 16 { d32^( bes^) } | % 140
% \tuplet 5/4 4 { d16 bes d bes d   bes d bes d bes   d b d b d   b d b d b } | % 141
  \repeat tremolo 8 { d32_( bes) } \repeat tremolo 8 { d32_( b_) } | % 141
% \tuplet 5/4 4 { dis16 b dis b dis   b dis b dis b   dis c dis c dis   c dis c dis c } | % 142
  \repeat tremolo 8 { dis32_( b_) } \repeat tremolo 8 { dis32_( c } | % 142
% \tuplet 5/4 4 { dis16 c dis c dis   c dis c dis c   dis cis dis cis dis  b dis b dis b) } | % 143
  \repeat tremolo 12 { dis32 c_) } \repeat tremolo 4 { b_( dis_) } | % 143
% \tuplet 5/4 4 { dis16( a dis a dis   a dis a dis a)   dis gis, dis' gis, dis'  fis, dis' fis, dis' fis, } | % 144
  \repeat tremolo 8 { dis32_( a_)^\) } \repeat tremolo 4 { dis32_( gis,_) } \repeat tremolo 4 { dis'32_( fis,_) } | % 144
  \repeat tremolo 16 { e'32^( e,^) } | % 145
% r2 r4 <e gis>4-\p | % 146
  \clef bass
% \tuplet 5/4 4 { b,16-\ppp-\<( e, b' e, b'  e, b' e, b' e,)   b'-\pp( e, b' e, b') } \clef treble <e' gis>4-\p | % 146
  \repeat tremolo 8 { b32-\ppp-\<( e,) } \repeat tremolo 4 { b'32-\pp( e,) } \clef treble <e' gis>4 | % 146
  <d g! b>4.-\accent <d g b>8 <d g b>2 |
% <e gis b>1-\pp | % 148
% \tuplet 5/4 4 { b'16-\ppp-\<( gis e b' gis   e b' gis e b'  gis-\pp e b' gis e b' gis e b' gis) } | % 148
% \repeat tremolo 16 { b'32-\ppp-\<^( <gis e>^) } | % 148
  <<
     { \voiceOne
       b'1-\ppp-\<  | % 148
     }
     \new Voice
     { \voiceTwo
       \repeat tremolo 16 { e,32-\shape #'((0.0 . -1.2) (0.0 . -0.6) (0.0 . -0.9) (0.0 . -1.5))( \once \override NoteColumn.force-hshift = #4.0 gis) } | % 148
     }
  >>
  \oneVoice
  r2-\! r4 <gis, b>4 | % 149
  <b dis>4. <b dis>8 <b dis>2 |
  <b e>1 |
  <<           { \voiceOne r1 | gis'2 gis4 fis | gis1( | e1) | e1 | e1 | e1~ | e1\fermata }
    \new Voice
    { \voiceTwo
      gis2-\pp( e |
      b2) e |
      e4-\shape #'((0 . -1.6) (0 . -1.6) (0 . -1.6) (0 . -1.6))( gis,) a b |
      cis2-\shape #'((0 . -1.7) (0 . -1.7) (0 . -1.7) (0 . -1.7))( b-\>) |
      b1-\ppp |
      b1 |
      b1~ |
      b1
    }
  >>
  \bar "|."
}

accordeon_r = \relative c'' {
  \global
  s1
  R1*2
  <d, g b>2-\pp
  <<
%      { \voiceOne \tuplet 5/4 4 { b'16(_\markup { \dynamic ppp \italic ", molto legato" }_\< g b g b   g b g b g) } | }
   { \voiceOne \repeat tremolo 8 { b'32^\markup { \dynamic ppp \italic "sempre molto legato" }^\<(\( g) } | }
   \new Voice { \voiceTwo d4 d | }
  >>
  \oneVoice
% \tuplet 5/4 4 { b'16(-\pp g b g b   g b g b g  b g b g b   g b g b g } | % 4
  \repeat tremolo 16 { b'32( g } | % 4
% \tuplet 5/4 4 { b16 g b g b  g b g b g   b-\< g b g b  fis a fis a fis } | % 5
  \repeat tremolo 12 { b32^\pp g) } \repeat tremolo 4 { fis-\<( a) } | % 5 
% <d, g>2_\pp <d g>4 <d g>4 |
% <g b>1~ |
% <g b>2.(\< <f a>4)\! |
% g1)-\sfz |
% \tuplet 7/4 4 { g16)_\sfz( e g e g e g   e g e g e g e   g e g e g e g  e g e g e g e } | % 6
  \repeat tremolo 16 { g32_\sfz( e } | % 6
% \tuplet 7/4 { g16 e g e g e g } e2.) | % 7
  \repeat tremolo 4 { g32 e~ } e2.)\)    | % 7
  g1-\pp~ |
  g1~ |
  g1~ |
  g1~ |
  g1~ |
  g1~ |
  g1~ |
  g2 f4 f |
  es1 |
  r1  |
  r8 es_\f es2 es4 |
  aes1_\sfz~ |
  aes4 b, r2 |
  R1*4
  %
  % bar 25
  %
  a'2_\pp~ a4. g8 |
  fis4.( e8) << { \voiceOne fis2   | \clef bass fis4. e8 d4 cis8 b8 | a2 r2 | fis 2 }
               \new Voice { \voiceTwo d'4. a8 | fis2 r | r2 d_\f~ | d2 }
            >>
  \oneVoice
  <g e>4 <g e> | % 29
  <a fis>2 <b g>4 r4 | % 30
  a1_\markup { \left-align { \italic "dim." } }~ | % 31
  a1  |                                            % 32
  R1*2
% c1_\markup { \center-align { \italic "dim." } }~ |
% c1~ |
% c1~ |
% c4_\pp r4 r2 \bar "||"
  \key bes \major
  bes1_\p   |
  bes4 r es,2 |
  f4 fis4 g2~ |
  g4 bes es_\> d |
  f_\pp c d4._\< bes8_\! |
  \tuplet 3/2 { g8( f g } \tuplet 3/2 { e8 f g } \tuplet 3/2 { a8 g a } \tuplet 3/2 { f8 g a } |
  \clef treble
  bes8 d f4_\>~ f8 g)_\! f8 es |
  d2 \clef bass \tuplet 3/2 { g,8( c bes } \tuplet 3/2 { a8 bes g } |
  \tuplet 3/2 { f8 g a } \tuplet 3/2 { g8 f es } d4) r4 |
  g4_\p a8 bes8 c2( |
  d4)_\> \clef treble g_\pp bes,4. d8 |
  c4 es d2~ |
  d4 es << { \voiceOne f2^\mf^\> | r2^\! d4. d8 |
                        \tuplet 3/2 { es8( f g } \stemNeutral \tuplet 3/2 { f8 es d } \tuplet 3/2 { c8 d es } \tuplet 3/2 { d8 c bes! } | aes2) }
              \new Voice { \voiceTwo g4( a | bes4 c) b c8 b | c4 s2. }
  >>
  \oneVoice g4_\markup { \italic "con espressione" } a!8 b | % 50
  c2. bes4 | % 51
  d4 c \clef bass es,4 f8 g | % 52
  a2 f | % 53
  \tuplet 3/2 4 { fis8( e fis d8 e fis) } g2 | % 54
  << { \voiceOne es'!2( d4) f! |
                 \tuplet 3/2 4 { c8( bes c a8 bes c } d2 | d4) e r e | es4 c\rest r2 | }
    \new Voice { \voiceTwo \tuplet 3/2 4 { a8( g a fis8 g a)_\> } bes4 r_\! |
                      r2 c2(_\pp~ | c4~ \tuplet 3/2 { c8 bes a) } bes2 | c2 f,4 e\rest | }
  >>
  %
  % bar 59
  %
  \oneVoice
  f1 |
  f1~ |
  f4 r4 r8 <f c>8_\mf_\< a <g bes> |
% <f c>4_\! \tuplet 5/4 4 { cis16(-\pp-\< e cis e cis  e cis e cis e  cis e cis e cis } | % 62
  <f c>4_\! \repeat tremolo 12 { cis32_(-\pp-\< e } | % 62
% <f c>4_\! <e cis>2.~ |
% <e cis>2 r8 <e a>8_\mf_\< <a cis> <a b> |
% \tuplet 5/4 4 { e16 cis e cis e   cis e cis e cis-\!) } r8 <e a>8_\mf_\< <a cis> <a b> | % 63
  \repeat tremolo 8 { cis32 e-\!) } r8 <e a>8-\mf-\< <a cis> <a b> | % 63
% <a e>4 <a fis>2._\markup { \right-align { \italic "cresc." } } |
% <a e>4 \tuplet 5/4 4 { fis16-\pp-\<( a fis a fis   a fis a fis a   fis a fis a fis-\!) } | % 64
  <a e>4 \repeat tremolo 12 { fis32-\pp-\<( a-\!) } | % 64
  \clef treble
  <gis gis'>2_\p <b e>4 gis |
% b2 <b cis>4 <a dis> | % 66
% b2 \tuplet 5/4 4 { b16(-\pp cis b cis b)   dis( a dis a dis) } | % 66
  b2 \repeat tremolo 4 { b32(-\pp cis) } \repeat tremolo 4 { dis( a) } | % 66
% << { \voiceOne e'1^\markup { \right-align { \italic "dim." } }~ | e2 }
%    \new Voice { \voiceTwo gis,2 r | s2 }
% >>
% \tuplet 5/4 4 { gis,16-\pp( e' gis, e' gis,   e' gis, e' gis, e'~) } e2~ | % 67
  \repeat tremolo 8 { gis32-\pp( e')~ } e2~ | % 67
  e2 dis2_\pp~ |
  dis1 |
  d!1~  |
  d4 r2. | % 71
  r2 \clef bass
  << { \voiceOne b2(^\pp^\< | cis2)^\! dis4 r | b1~ | b4 r4 r2 | s1 | s4 }
    \new Voice { \voiceTwo e,2_\pp_\<~ | e2_\! e2_\> | e1_\pp~ | e1~ | e1~ | e4 }
  >>
% \oneVoice <e gis b>2 <e g b cis>4 | % 77
% <d g b d>2 <d g b d>4 r4 | 
  \oneVoice
%            \tuplet 5/4 4 { gis16( e gis e gis  e gis e gis e)  gis( e gis e gis) } | % 77 
             \repeat tremolo 8 { e32( gis) } \repeat tremolo 4 { e32( gis) } | % 77
% \tuplet 5/4 4 { d!16( g! d g d   g d g d g)   d( g d g d) } \tuplet 3/2 { b'8 c a } | % 78
  \repeat tremolo 8 { d!32( g!) } \repeat tremolo 4 { d32( g) } \tuplet 3/2 { b8 c a } | % 78
  g8-\< a b c e4-\! d | % 79
% r4 d2_\markup { \left-align { \italic "dim." } } <bes d>4 | % 80
% <aes des>2 <g des'>4 r | % 81
% r4 d2-\markup { \left-align { \italic "dim." } } \tuplet 5/4 { bes16( d bes d bes) } | % 80
  r4 d2-\markup { \left-align { \italic "dim." } } \repeat tremolo 4 { bes32( d) } | % 80
% \tuplet 5/4 4 { des16( aes des aes des   aes des aes des aes)   des( g, des' g, des') } r4 | % 81
  \repeat tremolo 8 { des32( aes) } \repeat tremolo 4 { des32( g,) } r4 | % 81
  R1*1
  des'1 | % 83
  c2. r8 <ges a c>8 | % 84
  <f bes des>4. <des f bes>8-\> <f bes des>4. <bes des f>8 |
  bes1-\! | % 86
% << { \voiceOne <bes g'>2. c'4\rest | }
%   \new Voice { \voiceTwo des,4 bes4\rest bes2\rest | }
% >>
% \oneVoice
% \tuplet 5/4 4 { bes16( g' bes, g' bes,   g' bes, g' bes, g'  bes, g' bes, g' bes,) } r4 | % 87
  \repeat tremolo 12 { bes32( g') } r4 | % 87
  r2
  << { \voiceOne s2 | s1 | \stemDown \tupletDown \tieDown g,4\rest \tuplet 3/2 4 { des'8 c ces bes a aes } g4~ |
      g2 \stemUp \tupletUp \tieUp f'2(~ | f8 c \stemDown \tupletDown \tieDown \tuplet 3/2 { <bes des>8 <a c> <aes ces> } <g bes>2~ |
      <g bes>4) <es g>8 <f aes> g4 } % \slurDown \stemUp \tupletUp \tieUp \tuplet 3/2 { bes8( a aes } | g4~ \tuplet 3/2 { g8 aes a } bes4) g'4\rest | }
    \new Voice { \voiceTwo \stemNeutral \tupletNeutral f'!2_\pp_\<_\accent(~ | \tuplet 3/2 { f8^\! ges fes } \tieUp es2.~ |
                 \stemUp \tupletUp \tieUp es2~ es8 d es4~ | \tieDown es2~ \stemDown \tupletDown es8 d! es4~ |
                 es2) \stemUp \tieUp \slurDown es4 bes'8^( g^) | es2~ es8 bes } % \stemDown \tieDown \slurDown g4~ | g8( f) g bes g2 |}
  >>
  \oneVoice \tuplet 3/2 { bes8( a aes } |
  g4~ \tuplet 3/2 { g8 aes a } bes4) r4 |
  es2~_\pp_\<\( \tuplet 3/2 { es8 f_\! d } es4~ |
% \tuplet 3/2 { es8 d des } c4~ \tuplet 3/2 { c8 bes g } aes4~ | % 96
% aes4) \clef bass g2 f4 | % 97
% \tuplet 3/2 { es8 d des } c4~ \tuplet 3/2 { c8 bes g } \tuplet 5/4 { es16-\markup { \left-align \italic "subito" \dynamic ppp }  aes es aes es } | % 96
  \tuplet 3/2 { es8 d des } c4~ \tuplet 3/2 { c8 bes g } \repeat tremolo 4 { es32-\markup { \left-align \italic "subito" \dynamic ppp }(  aes } | % 96
% \tuplet 5/4 4 { aes16-\< es aes es aes)   g( es g es g   es g es g es)  f( es f es f)-\! } | % 97
  \repeat tremolo 4 { aes32-\< es)\) } \repeat tremolo 8 { g32( es) } \repeat tremolo 4 { f32( es)-\!~ } | % 97
  es1-\>\( \bar "||" % 98
  \key g \major
% << { \voiceOne \stemDown \tupletDown \slurDown \tieDown d2)_\! b2\rest | s1 | s1 | }
%   \new Voice { \voiceThree e!1~ | e1~ | e2 e2\rest | }
% >>
% \oneVoice
% \tuplet 5/4 4 { d16-\ppp-\< e d e d  e d e d e)~ } e2-\pp~ | %  99
  \repeat tremolo 8 { d32-\ppp-\<( e)\)~ } e2-\pp~ | % 99 
  e1~ |                                            % 100
  e2 r |                                           % 101
  R1*3
  r4 b'4-\p b2 | % 105
  b2  b   | % 106
  cis1   | % 107
  cis1   | % 108
  \clef bass
  es,,1_\p^\( | % 109
% \tuplet 5/4 4 { es16( ais es ais es   ais es ais es ais) } es2_\)  | % 110
  \repeat tremolo 8 { ais32_( es_)~ } es2^\) | % 110
% << { \voiceOne s1 | ais,2 d2\rest | }
%  \new Voice { \voiceTwo es,1_\p~ | es1 | }
% >>
% \oneVoice
  es1~ | % 111
  es2 r2 | % 112
  cis''2( c)    | % 113
  a!2 bes-\> | % 114
  c1-\!-\markup { \italic "dim." } | % 115
  cis1 | % 116
  \repeat tremolo 16 { d32(-\ppp-\< g,) } | % 117
  \repeat tremolo 12 { d'32(-\! g,) } \repeat tremolo 4 { d'32( g,) } | % 118
  dis'1 | % 119
  dis2~ \repeat tremolo 8 { dis32( a) } | % 120
  b,4(_\markup { \dynamic p \italic "con espressione" } cis) dis e8( fis) | % 121
  <<
    { \voiceOne a2 }
    \new Voice { \voiceTwo e4( fis) }
  >>
  \oneVoice gis4
  <<
               { \voiceOne a8( b) }
    \new Voice { \voiceTwo fis4 }
  >>
  <<
     { \voiceOne
        \repeat tremolo 8 { a32-\ppp-\<^(^\( dis^) } \repeat tremolo 8 { a32^( cis^) } | % 123
        \repeat tremolo 16 { a32-\pp_( b_) } | % 124
        \repeat tremolo 8 { gis32^( b^) } \repeat tremolo 8 { e,32^( b'^) } | % 125
     }
     \new Voice
     { \voiceTwo
        e,1~   | % 123
        e1~    | % 124
        e2  g,2\rest   | % 125
     }
  >>
  \oneVoice
  \repeat tremolo 16 { e'32^( b'^) } | % 126
% \tuplet 5/4 4 { e,16 a e a e   a e a e a  e a e a e  a e a e a) } | % 127
  \repeat tremolo 16 { e,32^( a^) } | % 127
% \tuplet 5/4 4 { b,16( a' b, a' b,  a' b, a' b, a')~ } a4 gis | % 128 
  \repeat tremolo 8 { b,32^( a'~ } a4^) gis | % 128
  fis2 e4\) r | % 129
  R1*1
% <<
%   { \voiceOne a1_\p~ | a1_\markup { \italic "dim." } | d,1_\pp | }
%   \new Voice { \voiceTwo e1~ | e2 \tieUp g2~ | \stemUp g2 f2\rest | }
% >>
% \oneVoice
% \tuplet 5/4 4 { a16-\ppp-\<( e a e a   e a e a e   a e a e a   e a e a e } | % 131
  \repeat tremolo 16 { a32-\ppp-\<_(^\( e } | % 131
% \tuplet 5/4 4 { a16-\markup { \dynamic p \italic "dim." } e a e a   e a e a e_)   a_( g a g a  g a g a g } | % 132
  \repeat tremolo 8 { a32-\markup { \dynamic p \italic "dim." } e_) } \repeat tremolo 8 { a32_( g_) } | % 132
% \tuplet 5/4 4 { g16-\pp_( d g d g   d g d g d_)~ } d2) | % 133
  \repeat tremolo 8 { g32-\pp_( d~ } d2_) | % 133
  d1 | % 134
% d1 | % 135
% \tuplet 5/4 4 { d16(-\ppp-\< d' d, d' d,   d' d, d' d, d'  d,-\pp d' d, d' d,  d' d, d' d, d' } | % 135
  \repeat tremolo 16 { d32-\ppp_( d' } | % 135
% d2 d |
% \tuplet 5/4 4 { d,16 d' d, d' d,   d' d, d' d, d'_)   d,( b' d, b' d,  b' d, b' d, b'~ } | % 136
  \repeat tremolo 8 { d,32 d'_) } \repeat tremolo 8 { d,_( b'~ } | % 136
  b1_) |
  bes1\) |
  a1 | % 139
% <<
%   { \voiceOne d2_\pp d4 d | bes2 c4\rest f | es1 | }
%   \new Voice { \voiceTwo aes,1 | g1~ | g1 | }
%   \new Voice { \voiceFour fis1~  | fis1  | s1 | }
% >>
% \oneVoice
% \tuplet 5/4 4 { fis16-\ppp-\<( aes d fis, aes   d fis, aes d fis,)  aes-\pp( d fis, aes d)  fis,( aes d fis, aes) } | % 140
% \repeat tremolo 8 { d,32-\ppp-\<( <aes' d>) } \repeat tremolo 4 { d,32-\pp( <aes' d>) } \repeat tremolo 4 { d,32( <aes' d>) } | % 140
% \tuplet 5/4 4 { bes16( fis aes bes fis   aes bes fis aes bes)   aes( bes aes bes aes)  f'( aes, bes f' aes,) } | % 141
% \repeat tremolo 8 { d,32( <g bes>) } \repeat tremolo 4 { d32( g) } \repeat tremolo 4 { d32( <f g>) } | % 141
  <<
    { \voiceOne
      \repeat tremolo 8 { aes32-\ppp-\<(  d) } \repeat tremolo 4 { aes32-\pp(  d) } \repeat tremolo 4 { aes32( d) } | % 140
      \repeat tremolo 8 { g,32( bes)) } g4~ \repeat tremolo 4 { g32( f) } | % 141
    }
    \new Voice
    { \voiceTwo
      d1~ | % 140
      d1   | % 141
    }
  >>
% \tuplet 5/4 4 { g es' g, es' g,   es' g, es' g, es'   g, es' g, es' g,   es' g, es' g, es'  } | % 142
  \repeat tremolo 16 { es32_( g_) } | % 142
  fis1~ |
  fis2 r2 |
% <b, b'>1_\pp | % 145
  \repeat tremolo 16 { b,32-\ppp-\<_( b'-\!_) } | % 145
% <b e gis b>2-\p <b e gis b> | % 146
% \tuplet 5/4 4 { b16( gis' b, gis' b,   gis' b, gis' b, gis')    b,( gis' b, gis' b,  gis' b, gis' b, gis') } | % 146
  \repeat tremolo 8 { b,32( gis') } \repeat tremolo 8 { b,32( gis') } | % 146
  d4.-\accent d8 d2 | % 147
% <e gis b>1-\pp | % 148
% \tuplet 5/4 4 { e16-\ppp-\<( gis b e, gis   b e, gis b e,   gis b e, gis b e, gis b e, gis-\!) } | % 148
% \repeat tremolo 16 { e32-\ppp-\<_( <gis b>_) } | % 148
  <<
    { \voiceOne
      \repeat tremolo 16 { b'32-\ppp-\<-\shape #'((0.0 . 1.1) (0.0 . 0.0) (0.0 . 0.0) (0.0 . 1.1))( \once \override Voice.NoteColumn.force-hshift = #3.0  gis) } | % 148
    }
    \new Voice
    { \voiceTwo
      e1 | % 148
    }
  >>
  << { \voiceOne e2-\p-\> e4 e-\pp | gis4. gis8 gis2 | gis1 | r2 b4.-\p( a8) |
                 gis2-\p \tieDown \stemDown e2-\pp~ | \tieUp e1~ | \stemUp e2.-\markup { \left-align \italic "dim." } e4 | e1-\ppp | e1 | e1~ | e1\fermata }
   \new Voice
   { \voiceTwo
      <e, b'>2 <e b'> |
      r1 | b'1~ | b1 |
      b4\rest \slurUp cis8-\<( dis8) \stemUp gis4-\! fis |
      \slurDown \stemDown s4 gis, a b |
      cis2-\shape #'((0 . -0.1) (0 . -1.0) (0 . -1.5) (0 . -2.5))( b2-\>~ |
      b1-\!~ | b1~ | b1~ | b1)
    }
   \new Voice
   { \voiceFour
     s1*4 |
     e,1~ |
     \override NoteColumn.ignore-collision = ##t
     e1-\shape #'((0 . -0.25) (0 . -0.8) (0 . -0.8) (0 . -0.1))~ |
     \once \override Voice.NoteColumn.force-hshift = #2.5 e1~ |
     e2 dis2 | e1~ | e1~ | e1\fermata
   }
  >>
  \bar "|." % END
}

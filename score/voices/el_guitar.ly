el_guitar = \relative c''' {
  \global
  s1 \bar ""
  \override Hairpin #'circled-tip = ##t
  g1\<~ | g1~ | g1~ | g2_\pp(~g4. fis8 | e4 b8 d8 g2~ | g2.) r4 |
  \override Hairpin #'circled-tip = ##f
  \compressFullBarRests { R1*4 }
  r2 g2_\p~ |
  g4. f8 es4 bes8 des8 |
  g1 |
  \compressFullBarRests { R1*2 }
  r2 g2_\p~ |
  g4. f8 es4 a,8 c8 |
  g'1~ | % 19
  g4 r4 r2 |
  \compressFullBarRests { R1*4 }
  %
  % bar 24
  %
  g1_\pp~ |
  g4. fis8 e4 a,8 d8 |
  a'1~ |
  a1~ |
  a1_\markup { \center-align { \italic "cresc." } } |
  %
  % bar 29
  %
  <a,, a' d fis>2-\f <g g' c>4 <g' c e> | % 29
  <a d fis>2 <g c e>4 <g c e>4 |
% <a d fis>2 \tuplet 6/4 { g,16(^\( a b c d e) } g16( gis a b)^\) | % 30
  c1(~_\markup { \left-align { \italic "dim." } } |
  c1~ |
  c1~ |
  c1 \bar "||"
  \key bes \major
  bes4)_\ppp r4 r2 |
  \compressFullBarRests { R1*5 }
  %
  % bar 41
  %
  bes2_\mf\> bes4_\! r4 |
  bes4(_\markup { \italic "con tenerezza espressiva" } c8 d8 es2~ |
  es4 d4 f4 es4 |
  d2 c4) bes4~ |
  bes8 d8 g,4(~ \tuplet 3/2 { g8 f g } \tuplet 3/2 { e8 f g } |
  \tuplet 3/2 { a8 g a } \tuplet 3/2 { f8 g a } bes8 d f4~ |
  f4 g) f es |
  d1 |
  %
  % bar 49
  %
  c,2_\mf\> es4_\! r4 |
  des4(_\markup { \italic "con espressione" } es8 f8 g2~ |
  g4 f aes4 g |
  f2 es4. d8) |
  cis8 e8 a,4(~ \tuplet 3/2 { a8 g a } \tuplet 3/2 { f8 g a } |
  bes2~ \tuplet 3/2 { bes8 a bes } \tuplet 3/2 { g8 a bes } |
  c2) d2_\> |
  \tuplet 3/2 { f8(_\!\pp e d) } f4(~ \tuplet 3/2 { f8 e f } \tuplet 3/2 { d8 e f } |
  g2~ \tuplet 3/2 { g8 f g } \tuplet 3/2 { e8 f g } |
  a2~ \tuplet 3/2 { a8 g a } \tuplet 3/2 { f8 g a } |
  d,8_\p) f bes,4(~ \tuplet 3/2 { bes8 a bes } \tuplet 3/2 { g8 a bes } |
  c4 es8 g) c,4 r8 es8 |
  f4 r4 r8 a,8_\mf\< c d |
  dis4_\! e2.~ |
  e2 r8 e8_\mf\< a, b |
  <cis e>4 <c e>2._\markup { \center-align { \italic "cresc." } } |
  <b e gis>2_\p <gis b e>4 <gis b>4 |
  b2 <b cis>4 <a dis>4 |
  b1_\pp~ |
  b1 |
  b1_\pp~ |
  b1~ |
  b4 <a cis>2.~ |
  <a cis>2 << { \voiceOne b2_\< | b1^\accent_\> | }
              \new Voice { \voiceTwo gis2 | r2 a2 | }
  >>
  \oneVoice
  <gis b>1_\pp~ |
  <gis b>1~ |
  <gis b>4 r4 r2 |
  \compressFullBarRests { R1*1 }
  g!4. a8 b4~ \tuplet 3/2 { b8 c a } |
  g8_\< a b c_\! e4 d |
  r4 d2_\markup { \center-align { \italic "dim." } } <bes d g>4 |
  <aes des f>2 <g des' es>4\laissezVibrer r4 |
  \compressFullBarRests { R1*1 }
  r2 r4 r8 ges'8_\pp |
  ges1 |
  f4\laissezVibrer r4 r2 |
  r4 e2.^\accent_\pp\<(~ |
  \tuplet 3/2 { e8 f_\! es } des2.~ |
  des1_\> |
  des2_\pp~ des4. c8 |
  des2~ des4. c8 |
  des2~ des4. c8 |
  des2.~ \tuplet 3/2 { des8 f es } |
  des2.) des8 c |
  bes1_\<~ |
  bes4_\! aes8 g_\> aes2 |
% aes4_\! r4 r4 aes4_\pp |
  aes1-\pp |
  aes1 |
  aes2 aes \bar "||"
  \key g \major
  g1~ |
  g1~ |
  g2 r2 |
  \compressFullBarRests { R1*3 }
  %
  % bar 105
  %
  r4 fis'4_\p fis2~ |
  fis2 b |
  gis1~ |
  gis1_\> |
  g2_\! r2 |
  \compressFullBarRests { R1*3 }
  %
  % bar 113
  %
  r2 c,4(_\mf des) |
  es4_\< f8( g)_\! bes2( |
  aes1)_\markup { \right-align { \italic "dim." } } |
  g1~ |
  g4_\pp r4 r2 |
  \compressFullBarRests { R1*5 }
  %
  % bar 123
  %
  a,4(_\markup { \dynamic p \italic "con espressione" } b) cis dis8( e) |
  gis2 fis2 |
  gis2._\pp( e4) |
  b1 |
  \compressFullBarRests { R1*2 }
  %
  % bar 129
  %
  b4(_\markup { \dynamic p \italic "con espressione" } cis) d e8( fis) |
  a2( gis |
  fis2 e~ |
  e) es_\markup { \italic "dim." } |
  d1_\pp |
  g2._\markup { \italic "con espressione" } fis!4 |
  e2 b4 d |
  b2 b2 |
  b2 gis4_\< fis_\! |
  eis1( |
  <fis cis'>1)_\> |
  r1_\! |
  %
  % bar 141
  %
  <<
    { \voiceOne g'1_\markup { \dynamic pp \center-align { \italic "cresc." } }~ | g1 | fis1_\markup { \center-align { \italic "dim." } }~ | fis2 b2 | }
    \new Voice { \voiceTwo d,1~ | d2 g,4 gis | a2.( fis4 | cis'4 c) b a | }
  >> 
  \oneVoice
  <gis gis' b>1_\pp |
  r2 r4 <gis b>_\p |
  <g! b>4.^\accent <g b>8 <g b>2 |
  \compressFullBarRests { R1*1 }
  %
  % bar 149
  %
  <gis b e gis>2_\p <gis b e gis>2 |
  %
  \compressFullBarRests { R1*1 }
  %
  % bar 151
  %
  r2 gis'4.(_\p fis8) |
  <<
    { \voiceOne e4_\< gis8( a8) b2(~_\pp | b1~ | b4 gis) e r | }
    \new Voice { \voiceTwo b2( gis | e) gis | b1 | }
  >>
  \oneVoice
  <gis b e gis>1_\ppp |
  <gis b e gis>1 |
  <<
    { \voiceOne <e' gis>1 | r1 | }
    \new Voice { \voiceTwo <gis, b>1~ | <gis b>2 r2 | }
  >>
  \oneVoice
  r1^\fermata
}

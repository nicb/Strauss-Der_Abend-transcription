children_B = \relative c'' {
  \global
  \autoBeamOff
  s1 \bar ""
  r1 
  r1 b2^\pp b4 b d1^\markup { \italic "(senza respiro udibile)" }~d~d2 d~d des2 des1~des~des~des~des~des~des( c1~c2)
  %
  %             v this is really a d in the original score
  bes4 bes a1~a a1 aes2 aes es'1
  \compressFullBarRests { R1*4 }
  %
  % bar 26
  %
  a,1^\pp a2 a2 a1^\markup { \italic "cresc." }~a1~a1~a1
  a2^\ff g4 g4 fis2 e4 e4 d1~ \bar "||" \key bes \major d4 r4 r2
  \compressFullBarRests { R1*27 }
  %
  % bar 63
  %
  r2 r8 a'^\< a a |
  e'4^\! c!2.^\markup { \center-align { \italic "cresc." } } |
  %
  % bar 65
  %
  e2^\p b4 gis4 gis2 gis4 fis4
  e1^\markup { \italic "dim." }~e2 fis2^\pp~fis1 e1
  e1 fis2 gis2^\< e2^\! dis4^\> dis4 e1^\pp~e1~e4 r4 r2 r1
  %
  % bars 78-98 (20 bars)
  %
  \compressFullBarRests { R1*21 }
  \bar "||" \key g \major
  %
  % bar 99
  %
  g1^\pp~g~g~g~g~g2 fis2
  %
  % bar 105
  %
  r1 r2 fis2^\p
  cis'1 cis1 cis1 cis1 cis1 cis2 r2
  %
  \compressFullBarRests { R1*4 }
  %
  % bar 117
  %
  d1^\pp d,2. g4 b1 b2 b2 b1(~b1 a2.) r4
  %
  \compressFullBarRests { R1*3 }
  %
  % bar 127
  %
  \autoBeamOn
  a4(^\markup { \dynamic "p" \italic "con espressione" } b4) cis4 dis8( e8)
  gis2( fis2~fis2 e2 dis1)^\markup { \italic "dim." } e4^\pp r4 r2 r1
  %
  % bar 133
  %
  g,1^\pp d1 r1 r1 gis1~( gis1 a1 bes1~bes2) b2~^\markup { \italic "cresc." } b2 c2^\sf~c2 cis4^\markup { \italic "dim." } b4 a2 gis4 fis4 e1^\pp
  %
  % bar 146
  %
  e2^\p e2
  %
  \compressFullBarRests { R1*2 }
  %
  % bar 149
  %
  r2 r4 e4^\pp |
  gis4. gis8 gis2 |
  gis1 |
  %
  % bar 152
  %
  b4.( a8) gis4 \autoBeamOn cis,8( dis8) |
  e1^\pp~ | e1 | e2 r2 |
  e1^\ppp | e1 | e1~ | e1^\fermata \bar "|."
}

children_B_text = \lyricmode {
  Strah -- len der Gott!
  Sen -- ke strah -- len der Gott!
  Strah -- len der Gott!
  Strah -- len -- der Gott!
  Sen -- ke den Wa -- gen hi -- nab!
  Er -- kennt dein Herz sie?
  The -- tys, die gött -- li -- che winkt.
  The -- tys, The -- tys, die gött -- li -- che winkt.
  Ru -- het!
  Ihr folgt die sü -- sse Lie -- be.
  Phö -- bus, der lie -- ben -- de ruht.
  Ru -- het und -- lie -- bet!
  Phö -- bus, Phö -- bus, Phö -- bus, der lie -- ben -- de ruht.
  Phö -- bus,
  der lie -- ben -- de ruht.
  Ru -- het und -- lie -- bet!
  Phö -- bus ruht.
}

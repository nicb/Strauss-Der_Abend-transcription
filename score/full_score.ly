\version "2.18.2"

\include "include/revision.ly"
\include "include/header.ly"
\include "include/markups.ly"

\paper {
  #(set-paper-size "a3")
}

\include "voices/soprano.ly"
\include "voices/children_A.ly"
\include "voices/children_B.ly"
\include "voices/piano.ly"
\include "voices/el_guitar.ly"
\include "voices/accordeon.ly"
\include "voices/bs_clarinet.ly"
\include "voices/fixed_media.ly"

\score {
  <<
    \score_markups
    \new Staff \with {
      \override Staff.InstrumentName.self-alignment-X = #RIGHT  
      midiInstrument = "synth voice"
      instrumentName = "Soprano"
      shortInstrumentName = "Sop."
    } \new Voice = "Soprano" {  \sopranofirstpart \sopranosecondpart \sopranothirdpart \sopranofourthpart \sopranofifthpart }
    \addlyrics {
      \soprano_text_first_part
      \soprano_text_second_part
      \soprano_text_third_part
      \soprano_text_fourth_part
      \soprano_text_fifth_part
    }
    \new StaffGroup \with {
      midiInstrument = "choir aahs"
      instrumentName = \markup { \left-column { "Coro" "di Bambini" } }
      shortInstrumentName = "Coro"
    }
    <<
      \new Staff { \children_A }
      \addlyrics {
        \children_A_text
      }
      \new Staff { \children_B }
      \addlyrics {
        \children_B_text
      }
    >>
    \new Staff \with {
      midiInstrument = "distorted guitar"
      instrumentName = \markup { \left-column { "Chitarra" "Elettrica" } }
      shortInstrumentName = "Chit. El."
      midiMinimumVolume = #0.1
      midiMaximumVolume = #0.4
    } \new Voice = "el. guitar" { \el_guitar }
    \new StaffGroup \with {
      midiInstrument = "accordion"
      instrumentName = "Accordeon"
      shortInstrumentName = "Acc."
      \override StaffGrouper.staff-staff-spacing =
          #'((padding . 2)
             (stretchability . 30))
    } <<
      \new Staff \with
      {
        midiMinimumVolume = #0.05
        midiMaximumVolume = #0.85
      } { \accordeon_l }
      \new Staff \with
      {
        midiMinimumVolume = #0.05
        midiMaximumVolume = #0.85
      } { \accordeon_r }
    >>
    \new Staff \with {
      midiInstrument = "clarinet"
      instrumentName = \markup { \left-column { "Clarinetto" "Basso" } }
      shortInstrumentName = "Cl. Bs."
    } \new Voice = "bs. clar." { \clef bass \bs_clar }
    \new GrandStaff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Piano"
      shortInstrumentName = "Pf."
      pedalSustainStyle = #'bracket
    } <<
      \new Staff \with {
                    midiMinimumVolume = #0.15
                    midiMaximumVolume = #0.99
      } { \piano_RHa }
      \new Staff \with {
                    midiMinimumVolume = #0.15
                    midiMaximumVolume = #0.99
      } { \piano_RHb }
      \new Staff \with {
                    midiMinimumVolume = #0.15
                    midiMaximumVolume = #0.99
      } { \clef bass \piano_LHa }
    >>
    \new RhythmicStaff \with {
      instrumentName = "Fixed Media"
      shortInstrumentName = "F.Med."
    } \new Voice = "f. media" { \fixed_media }
  >>
  \layout {
    indent = 3\cm
    short-indent = 1.5\cm
    \context {
       \Score
       \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/20)
        \override Flag.stencil = #modern-straight-flag
    }
     \context {
        \Staff
        \RemoveEmptyStaves
        \override VerticalAxisGroup #'remove-first = ##t
     }
  }
  \midi { 
    \tempo 4 = 63
  }
}

score_markups = \new RhythmicStaff \with {
  \remove "Staff_symbol_engraver"
  \remove "Note_heads_engraver"
  \remove "Rest_engraver"
  \remove "Time_signature_engraver"
  \remove "Key_engraver"
  \remove "Clef_engraver"
} \relative c'' {
  \set Score.currentBarNumber = #0
  \set Score.markFormatter = #format-mark-box-numbers
  \tempo \markup { \large "Sehr ruhig (tranquillo assai)" }
  s1*15
  \mark \default % 1
  s1*10
  \mark \default % 2
  s1*17
  \mark \default % 3
  %
  % bar 49
  %
  s1*7
  \mark \default % 4
  %
  % bar 56
  %
  s1*6
  s1 \mark \default
  %
  % bar 62
  %
  s1*6
  \mark \default
  %
  % bar 75 [7]
  %
  s1*13
  \textLengthOn
  \mark \default
  \tempo \markup { \large \column { \line { "etwas lebhafter" } \line { "(poco più animato)" } } }
  s1
  %
  % bar 79
  %
  s1*4
  \tempo \markup { \large \column { \line { "wieder ruhiger" } \line { "(più tranquillo)" } } }
  %
  % bar 84 [8]
  %
  s1*3
  s1 \mark \default
  %
  % bar 99
  %
  s1*14
  s1 \mark \default
  %
  % bar 109
  %
  s1*9
  s1 \mark \default
  %
  % bar 121
  %
  s1*11
  s1 \mark \default
  %
  % bar 133
  %
  s1*11
  s1 \mark \default
  %
  % bar 139
  %
  \textLengthOn
  s1*5
  s1
  \tempo \markup { \large \column { \line { "etwas beschleuningen" } \line { "(poco animato)" } } }
  %
  % bar 143
  %
  s1*3
  s1
  \tempo \markup { \large \column { \line { "wieder zurückhaltend" } \line { "(ritardando)" } } }
  s1
  s1
  \tempo \markup { \large \column { \line { "Erstes Zeitmaß" } \line { "(Tempo primo)" } } }
  s1 \mark \default
  \tempo \markup { \large \bold \column { \line { "immer ruhiger" } \line { "(sempre più tranquillo)" } } }  
}

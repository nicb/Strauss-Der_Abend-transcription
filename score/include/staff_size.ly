staffSize = #(define-music-function (parser location new-size) (number?)
#{
  \set Staff.fontSize = #new-size  
  \override Staff.StaffSymbol #'staff-space = #(magstep new-size)
  \override Staff.StaffSymbol #'thickness = #(magstep new-size)
#})

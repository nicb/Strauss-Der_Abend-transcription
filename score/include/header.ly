\paper {
  ragged-last-bottom = ##f
  system-separator-markup = \slashSeparator
}

\header {
  title = "Der Abend"
  subtitle = "nach einem Text von Friederich Schiller"
  composer = "Richard Strauss (1897)"
  arranger = "trascrizione di Nicola Bernardini (2016)"
  % instrument = "per soprano, coro di bambini, pianoforte, chitarra elettrica, accordeon, clarinetto basso e fixed media"
  tagline = \markup { \revision " music engraved with Lilypond 2.18.2" }
  copyright = \markup { \revision " music engraved with Lilypond 2.18.2 - CC BY-SA 4.0 International License" }
  
}

global_timesig = {
  \numericTimeSignature
  \time 4/4
  \compressFullBarRests
}
global_key_tempo = {
  \key g \major
}
global = { \global_timesig \global_key_tempo }

\include "include/martellato.ly"
\include "include/slash.ly"

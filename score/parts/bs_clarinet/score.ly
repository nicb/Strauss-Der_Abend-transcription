\version "2.18.2"

\include "include/revision.ly"
\include "include/header.ly"
\include "include/markups.ly"

\paper {
  #(set-paper-size "b4")
}

\include "voices/bs_clarinet.ly"

\header {
  instrument = "clarinetto basso"
}

\score {
  <<
    \score_markups
    \new Staff \with {
      midiInstrument = "clarinet"
      instrumentName = \markup { \left-column { "Clarinetto" "Basso" } }
    } \new Voice = "bs. clar." { \transpose bes c'' { \clef treble \bs_clar } }
  >>
  \layout {
    indent = 3\cm
    short-indent = 1\cm
    \context {
       \Score
       \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/20)
        \override Flag.stencil = #modern-straight-flag
    }
     \context {
        \Staff
        \RemoveEmptyStaves
        \override VerticalAxisGroup #'remove-first = ##t
     }
  }
  \midi { 
    \tempo 4 = 63
  }
}

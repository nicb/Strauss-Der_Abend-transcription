\version "2.18.2"

\include "include/revision.ly"
\include "include/header.ly"
\include "include/markups.ly"

\paper {
  #(set-paper-size "b4")
}

\include "voices/el_guitar.ly"

\header {
  instrument = "chitarra elettrica"
}

\score {
  <<
    \score_markups
    \new Staff \with {
      midiInstrument = "distorted guitar"
      instrumentName = \markup { \left-column { "Chitarra" "Elettrica" } }
      midiMinimumVolume = #0.1
      midiMaximumVolume = #0.4
    } \new Voice = "el. guitar" { \transpose c c' \el_guitar }
  >>
  \layout {
    indent = 3\cm
    short-indent = 1\cm
    \context {
       \Score
       \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/20)
        \override Flag.stencil = #modern-straight-flag
    }
     \context {
        \Staff
        \RemoveEmptyStaves
        \override VerticalAxisGroup #'remove-first = ##t
     }
  }
  \midi { 
    \tempo 4 = 63
  }
}

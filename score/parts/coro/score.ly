\version "2.18.2"

\include "include/revision.ly"
\include "include/header.ly"
\include "include/markups.ly"
\include "include/staff_size.ly"

\paper {
  #(set-paper-size "b4")
}

\include "voices/soprano.ly"
\include "voices/children_A.ly"
\include "voices/children_B.ly"

\header {
  instrument = \markup { "Coro" "di Bambini" }
  tagline = ""
  copyright = ""
}

\score {
  <<
    \score_markups
    \new Staff \with {
      \staffSize #-4
      \override Staff.InstrumentName.self-alignment-X = #RIGHT  
      midiInstrument = "synth voice"
      instrumentName = "Soprano"
    } \new Voice = "Soprano" {  \sopranofirstpart \key bes \major  s1*27 \sopranothirdpart s1*20 \sopranofifthpart }
    \addlyrics {
      \set fontSize = #-4
      \soprano_text_first_part
      \soprano_text_third_part
      \soprano_text_fifth_part
    }
    \new StaffGroup \with {
      midiInstrument = "choir aahs"
      instrumentName = \markup { \left-column { "Coro" "di Bambini" } }
    }
    <<
      \new Staff { \children_A }
      \addlyrics {
        \children_A_text
      }
      \new Staff { \children_B }
      \addlyrics {
        \children_B_text
      }
    >>
  >>
  \layout {
    indent = 3\cm
    short-indent = 1\cm
    \context {
       \Score
       \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/20)
        \override Flag.stencil = #modern-straight-flag
    }
     \context {
        \Staff
        \RemoveEmptyStaves
        \override VerticalAxisGroup #'remove-first = ##t
     }
  }
  \midi { 
    \tempo 4 = 63
  }
}

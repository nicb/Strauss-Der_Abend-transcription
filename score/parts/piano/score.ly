\version "2.18.2"

\include "include/revision.ly"
\include "include/header.ly"
\include "include/markups.ly"

\paper {
  #(set-paper-size "b4")
}

\include "voices/piano.ly"

\header {
  instrument = "pianoforte"
}

\score {
  <<
    \score_markups
    \new GrandStaff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Piano"
      pedalSustainStyle = #'bracket
    } <<
      \new Staff \with {
                    midiMinimumVolume = #0.15
                    midiMaximumVolume = #0.99
      } { \piano_RHa }
      \new Staff \with {
                    midiMinimumVolume = #0.15
                    midiMaximumVolume = #0.99
      } { \piano_RHb }
      \new Staff \with {
                    midiMinimumVolume = #0.15
                    midiMaximumVolume = #0.99
      } { \clef bass \piano_LHa }
    >>
  >>
  \layout {
    indent = 3\cm
    short-indent = 1\cm
    \context {
       \Score
       \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/20)
        \override Flag.stencil = #modern-straight-flag
    }
     \context {
        \Staff
        \RemoveEmptyStaves
        \override VerticalAxisGroup #'remove-first = ##t
     }
  }
  \midi { 
    \tempo 4 = 63
  }
}

\version "2.18.2"

\include "include/revision.ly"
\include "include/header.ly"
\include "include/markups.ly"

\paper {
  #(set-paper-size "b4")
  bottom-margin-default = 6\cm
  last-bottom-spacing = #'((padding . 6\cm))
}

\include "voices/soprano.ly"

\header {
  instrument = "soprano"
  tagline = ""
  copyright = ""
}

\score {
  <<
    \score_markups
    \new Staff \with {
      \override Staff.InstrumentName.self-alignment-X = #RIGHT  
      midiInstrument = "synth voice"
      instrumentName = "Soprano"
    } \new Voice = "Soprano" {  \sopranofirstpart \sopranosecondpart \sopranothirdpart \sopranofourthpart \sopranofifthpart }
    \addlyrics {
      \soprano_text_first_part
      \soprano_text_second_part
      \soprano_text_third_part
      \soprano_text_fourth_part
      \soprano_text_fifth_part
    }
  >>
  \layout {
    indent = 3\cm
    short-indent = 1\cm
    \context {
       \Score
       \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/20)
        \override Flag.stencil = #modern-straight-flag
    }
     \context {
        \Staff
        \RemoveEmptyStaves
        \override VerticalAxisGroup #'remove-first = ##t
     }
  }
  \midi { 
    \tempo 4 = 63
  }
}

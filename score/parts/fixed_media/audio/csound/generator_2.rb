# generator 2

require 'abend'

SV = Abend::SystemVariables.new(68, 180, 'output/out-reverb-send_2.wav')

Abend::Note.include_module(Abend::Constants::D2)

nnote = 0

Notes =
[
  Abend::Note.new(nnote += 1, 0.0, 8.11, SV),
  Abend::Note.new(nnote += 1, 8.0, 9.05, SV),
  Abend::Note.new(nnote += 1, 19.0, 9.07, SV),
  Abend::Note.new(nnote += 1, 23.0, 9.04, SV),
  Abend::Note.new(nnote += 1, 35.0, 9.02, SV),
]

SV.header 

Notes.each do
  |n|
  n.to_csound
end

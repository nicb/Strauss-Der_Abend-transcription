;
; orchestra driver for Strauss Der Abend (included from orchestra.orc)
;
              nchnls = 8

ginumzaks     init 200

;
; 		               Global organization:
;
;                         instr 1, ...
;         (sound generators - writing into 6 different loc)
;                             |
;                             |	
;                     +-------+
;                     |
;                  zawm 1,2,3...,6
;                     |
;               instr 101-106 (init), instr 201-206 (static), 301-306 (spiral)
;            (6 movement engines)
;                     |
;     zkw 1-6(amp),101-106(x),151-156(y)
;                     |
;             +-------+-------+
;             |               |
;             |               |
;             v               v
;       instr 401-406     instr 1000
;        (48-ch room)     (8-ch rev send)
;
garevsp1			init	0
garevsp2			init	0
garevsp3			init	0
garevsp4			init	0
garevsp5			init	0
garevsp6			init	0
garevsp7			init	0
garevsp8			init	0

;
; speed of sound
;
givel		    	init	344

gixposoff		  init	 100
giyposoff		  init	 150

giroomfunc		init	 100

girescale     init  ampdb(6)     ; scale all signals by this factor
;
; the zak a variables hold the variables for the audio outputs
; of instruments 1-6, like this:
;
; zar[1]  = instr 1 audio output
; ...
; zar[n]  = instr n audio output
;
; the zak k variables hold the position variables in the
; following way:
;
; zkr[1]     = instr 1  (instr 1 dynamic and position controller) cur amp
; ...
; zkr[N]     = instr N    (instr N dynamic and position controller) cur amp
; zkr[xoff+1]= instr 1  (instr 1 dynamic and position controller) cur x position
; ...
; zkr[xoff+N]= instr N  (instr N dynamic and position controller) cur x position
; zkr[yoff+n]= instr 1  (instr 1 dynamic and position controller) cur y position
; ...
; zkr[yoff+n]= instr N  (instr N dynamic and position controller) cur y position
;
;
			        zakinit ginumzaks, ginumzaks
;			        ;
;			        ; 6 zak reading instruments, writing onto 6 separate channels
;			        ;
;			        instr 11, 12, 13, 14, 15, 16
;ifirst        init       10
;ioutzidx      =          p1 - ifirst
;			        iampcorr = ampdb(p4)
;			        ipitch = p5
;			        ifno = p6
;
;asig, ametro  diskin  ifno, ipitch
;asig          =       asig * iampcorr
;asig          linen   asig,0.005,p3,0.005
;
;              zawm    asig, ioutzidx
;              endin

              ;
              ; zak system initialization engines
              ;
              instr 101, 102, 103, 104, 105, 106
ifirst        init  100
index         =     p1-ifirst
ix            =     p4
iy            =     p5
ixindex       =     index+gixposoff
iyindex       =     index+giyposoff

              ziw   ix, ixindex
              ziw   iy, iyindex
              endin
							;
							; movement engines
						  ; line movement engines
							; (6 instruments)
							;
	            instr 201, 202, 203, 204, 205, 206
            	ifirst init 200	; first instrument number
              index = p1-ifirst
            	ixstart=p4
            	ixend=p5
            	iystart=p6
            	iyend=p7
            	ixindex=index+gixposoff
            	iyindex=index+giyposoff
              ieps = 3/kr
              itime = p3 - ieps ; remove just a tiny time to make sure we reach the target
            
kx            linseg	ixstart,itime,ixend,ieps,ixend
ky	          linseg	iystart,itime,iyend,ieps,iyend
            
              print p1, index, ixindex, iyindex, itime
                            
            	zkw	kx,ixindex
            	zkw	ky,iyindex

	            endin

    ;
    ; spiral space engines
    ;
    instr 301, 302, 303, 304, 305, 306
	  ifirst init 300	; first instrument number
    index  =    p1-ifirst
	  ixindex=index+gixposoff
	  iyindex=index+giyposoff

	; p4	 p5	     p6
	;speed  fase   modulo (distance from the center)

idur	          = p3	; movement duration 
ispeed_start    = p4	; rotation period start
ispeed_end      = p5	; rotation period start
iphasesine      = p6    ; fase iniziale distanza (parte coseno) [0..1]
imodulostart    = p7  ; initial distance from the center, in meters
imoduloend      = p8  ; final   distance from the center, in meters

  ;
  ; make sure iphasesine is [0..1]
  ;
                if iphasesine >= 0.0 igoto skipwrap
iphasesine      = iphasesine + 1.0
skipwrap:
iphasecosine    = iphasesine + 0.25
                if iphasecosine <= 1.0 igoto skipwrap2
iphasecosine    = iphasecosine - 1.0
skipwrap2:
  ;
  ; spiral expansion/shrink function
  ;
kmodulo          expon   imodulostart, p3, imoduloend
  ;
  ; speed accel/decel function
  ;
kspeed          expon   ispeed_start,p3,ispeed_end
kx              oscili  kmodulo, 1/kspeed,1,iphasecosine
ky              oscili  kmodulo, 1/kspeed,1,iphasesine

itime           times
                print p1, itime, index, ixindex, iyindex
ksample         timek
ktime           =       ksample/kr
kflag           =       ksample % 10000 ; print one every 10000 values

            if kflag != 0 kgoto skipprint
                fprintks "output/plots/spiral.data", "%8.4f %8.4f %8.4f %8.4f %8.4f\n", ktime, kmodulo, kspeed, kx, ky

skipprint:
	              zkw	kx,ixindex
	              zkw	ky,iyindex

endin

  ;
	;-------------------------------------------
	;	    ROOM INSTRUMENTS
	;-------------------------------------------
	;
	; 
	instr 401, 402, 403, 404, 405, 406
	ifirst	init	400				; first instrument number
	;
	; input indexings
	;
	index=p1-ifirst				    ; audio channels
	ixindex=index+gixposoff		; x position controls
	iyindex=index+giyposoff		; y position controls
  print p1, index, ixindex, iyindex
	;
	; p4	 p5
	;atten attnrev
	;
	idur  = p3
	iattadir = ampdb(p4)			; attenuazione di ampiezza [0]
 	iattarev = ampdb(p5)			; attenuazione riverbero [-20]
  iattroom = ampdb(p6)      ; attenuazione riflessioni dei muri [-6]
  istartdist = p7           ; starting distance radius (in m)
  ienddist   = p8           ; ending distance radius (in m)
  ifcfact    = 10000        ; scaling factor
  ifc0       = (sr/2)/ifcfact ; starting frequency cutoff
  ifc1       = 1000/ifcfact   ; ending frequency cutoff
  iafiltfact = (exp(ifc1)-exp(ifc0))/(ienddist-istartdist); factors of the log function
  ibfiltfact = exp(ifc0) - (iafiltfact*istartdist)        ;    "        "    "     "
	iroom      = giroomfunc		; table room def (set by engines)
	;
	; audio input
	;
asend	zar	index		    			; signal

asig	=		asend*iattadir
asend	=		0
		zacl	index,index		    ; clear input

kx		zkr		ixindex				; coming from the movement engines
ky		zkr		iyindex				; coming from the movement engines
kdist =     sqrt((kx^2)+(ky^2)) ; actual distance of the source
  ;
  ; distance air absorption
  ;
kdistflt = log((iafiltfact * kdist) + ibfiltfact)*ifcfact
asig       tone asig, kdistflt

ksample     timek
ktime       =       ksample/kr
kflag       =       ksample % 10000 ; print one every 10000 values

            if kflag != 0 kgoto skipprint
            fprintks  "output/plots/filtering.data", "%4d %9.4f %5.2f %5.2f\n", p1, ktime, kdist, kdistflt

skipprint:
      ;kt init 0
      ;kt = kt + 1/kr
      ;printks ">>>> room t = %8.4f x = %8.4f y = %8.4f\n", 0.2, kt, kx, ky

	                ;Inner room definition
	ixsp1  table 0,iroom     ;coordinate(in m) altoparlante 1 (LF)
	iysp1  table 1,iroom
	ixsp2  table 2,iroom     ;coordinate(in m) altoparlante 2 (RF)
	iysp2  table 3,iroom
	ixsp3  table 4,iroom     ;coordinate(in m) altoparlante 3 (LLF)
	iysp3  table 5,iroom
	ixsp4  table 6,iroom     ;coordinate(in m) altoparlante 4 (RRF)
	iysp4  table 7,iroom
	ixsp5  table 8,iroom     ;coordinate(in m) altoparlante 5 (LLF)
	iysp5  table 9,iroom
	ixsp6  table 10,iroom    ;coordinate(in m) altoparlante 6 (RRF)
	iysp6  table 11,iroom
	ixsp7  table 12,iroom    ;coordinate(in m) altoparlante 7 (LR)
	iysp7  table 13,iroom
	ixsp8  table 14,iroom    ;coordinate(in m) altoparlante 8 (RR)
	iysp8  table 15,iroom
	                ;DEFINIZIONE OUTER ROOM       
	ixmax table 16,iroom     ;larghezza max positiva (in m) 
	ixmin table 17,iroom     ;larghezza max negativa (in m) 
	iymax table 18,iroom     ;lunghezza max positiva (in m)
	iymin table 19,iroom     ;lunghezza max negativa (in m) 
	ix    table 20,iroom		; max coordinata x della sorgente
	iy    table 21,iroom		; max coordinata y della sorgente
	print ixsp1,iysp1,ixsp2,iysp2,ixmax,ixmin,iymax,iymin
	print ixsp3,iysp3,ixsp4,iysp4,ixsp5,iysp5,ixsp6,iysp6
 
	ideltaf = sr/2 - 2000
	kfilt = sr/2 + ideltaf*ky/iy
	af0 tone asig, kfilt 
    
	asigf = (ky >= 0 ? asig : af0) 
;     
	kxsp1  = kx-ixsp1                 
  kysp1  = ky-iysp1
	kxsp2  = kx-ixsp2
  kysp2  = ky-iysp2
	kxsp3  = kx-ixsp3
  kysp3  = ky-iysp3
	kxsp4  = kx-ixsp4
  kysp4  = ky-iysp4
	kxsp5  = kx-ixsp5
  kysp5  = ky-iysp5
	kxsp6  = kx-ixsp6
  kysp6  = ky-iysp6
	kxsp7  = kx-ixsp7
  kysp7  = ky-iysp7
	kxsp8  = kx-ixsp8
  kysp8  = ky-iysp8
	kxsp1q = kxsp1*kxsp1
  kysp1q = kysp1*kysp1
	kxsp2q = kxsp2*kxsp2
  kysp2q = kysp2*kysp2
	kxsp3q = kxsp3*kxsp3
  kysp3q = kysp3*kysp3
	kxsp4q = kxsp4*kxsp4
  kysp4q = kysp4*kysp4
	kxsp5q = kxsp5*kxsp5
  kysp5q = kysp5*kysp5
	kxsp6q = kxsp6*kxsp6
  kysp6q = kysp6*kysp6
	kxsp7q = kxsp7*kxsp7
  kysp7q = kysp7*kysp7
	kxsp8q = kxsp8*kxsp8
  kysp8q = kysp8*kysp8
	kxmax  = 2*(ixmax-kx)
	kymax  = 2*(iymax-ky)
	kxmin  = 2*(kx-ixmin)
	kymin  = 2*(ky-iymin)
	                          ;CALCOLO SEGNALE DIRETTO 
  kdsp1 = sqrt(kxsp1q+kysp1q) ; dist dir -> sp1
  kdsp2 = sqrt(kxsp2q+kysp2q) ; dist dir -> sp2
  kdsp3 = sqrt(kxsp3q+kysp3q) ; dist dir -> sp3
  kdsp4 = sqrt(kxsp4q+kysp4q) ; dist dir -> sp4
  kdsp5 = sqrt(kxsp5q+kysp5q) ; dist dir -> sp5
  kdsp6 = sqrt(kxsp6q+kysp6q) ; dist dir -> sp6
  kdsp7 = sqrt(kxsp7q+kysp7q) ; dist dir -> sp7
  kdsp8 = sqrt(kxsp8q+kysp8q) ; dist dir -> sp8
;	
;i muri sono numerati in senso orario a partire da quello di fronte 
;all'ascoltatore (m1)
;
; First-order reflections
;
; front wall (M1)
;
	km1      = ky+kymax           
	ksp1m1   = sqrt(kxsp1q+(km1*km1))   ;dist rif m1 -> sp1
	ksp2m1   = sqrt(kxsp2q+(km1*km1))   ;dist rif m1 -> sp2
	ksp3m1   = sqrt(kxsp3q+(km1*km1))   ;dist rif m1 -> sp3
	ksp4m1   = sqrt(kxsp4q+(km1*km1))   ;dist rif m1 -> sp4
	ksp5m1   = sqrt(kxsp5q+(km1*km1))   ;dist rif m1 -> sp5
	ksp6m1   = sqrt(kxsp6q+(km1*km1))   ;dist rif m1 -> sp6
	ksp7m1   = sqrt(kxsp7q+(km1*km1))   ;dist rif m1 -> sp7
	ksp8m1   = sqrt(kxsp8q+(km1*km1))   ;dist rif m1 -> sp8
;
; right wall (M2)
;
	km2      = ky+kymax           
	ksp1m2   = sqrt(kxsp1q+(km2*km2))   ;dist rif m2 -> sp1
	ksp2m2   = sqrt(kxsp2q+(km2*km2))   ;dist rif m2 -> sp2
	ksp3m2   = sqrt(kxsp3q+(km2*km2))   ;dist rif m2 -> sp3
	ksp4m2   = sqrt(kxsp4q+(km2*km2))   ;dist rif m2 -> sp4
	ksp5m2   = sqrt(kxsp5q+(km2*km2))   ;dist rif m2 -> sp5
	ksp6m2   = sqrt(kxsp6q+(km2*km2))   ;dist rif m2 -> sp6
	ksp7m2   = sqrt(kxsp7q+(km2*km2))   ;dist rif m2 -> sp7
	ksp8m2   = sqrt(kxsp8q+(km2*km2))   ;dist rif m2 -> sp8
;
; back wall (M3)
;
	km3      = kymin-ky
	ksp1m3   = sqrt(kxsp1q+(km3*km3))   ;dist rif m3 -> sp1
	ksp2m3   = sqrt(kxsp2q+(km3*km3))   ;dist rif m3 -> sp2
	ksp3m3   = sqrt(kxsp3q+(km3*km3))   ;dist rif m3 -> sp3
	ksp4m3   = sqrt(kxsp4q+(km3*km3))   ;dist rif m3 -> sp4
	ksp5m3   = sqrt(kxsp5q+(km3*km3))   ;dist rif m3 -> sp5
	ksp6m3   = sqrt(kxsp6q+(km3*km3))   ;dist rif m3 -> sp6
	ksp7m3   = sqrt(kxsp7q+(km3*km3))   ;dist rif m3 -> sp7
	ksp8m3   = sqrt(kxsp8q+(km3*km3))   ;dist rif m3 -> sp8
;
; left wall (M4)
;
	km4      = kxmin-kx
	ksp1m4   = sqrt(kxsp1q+(km4*km4))   ;dist rif m4 -> sp1
	ksp2m4   = sqrt(kxsp2q+(km4*km4))   ;dist rif m4 -> sp2
	ksp3m4   = sqrt(kxsp3q+(km4*km4))   ;dist rif m4 -> sp3
	ksp4m4   = sqrt(kxsp4q+(km4*km4))   ;dist rif m4 -> sp4
	ksp5m4   = sqrt(kxsp5q+(km4*km4))   ;dist rif m4 -> sp5
	ksp6m4   = sqrt(kxsp6q+(km4*km4))   ;dist rif m4 -> sp6
	ksp7m4   = sqrt(kxsp7q+(km4*km4))   ;dist rif m4 -> sp7
	ksp8m4   = sqrt(kxsp8q+(km4*km4))   ;dist rif m4 -> sp8
	                
  ;
  ; delays
  ;
  ; direct sound
  ;
  kdeldsp1 port kdsp1/givel,0.1
  kdeldsp2 port kdsp2/givel,0.1
  kdeldsp3 port kdsp3/givel,0.1
  kdeldsp4 port kdsp4/givel,0.1
  kdeldsp5 port kdsp5/givel,0.1
  kdeldsp6 port kdsp6/givel,0.1
  kdeldsp7 port kdsp7/givel,0.1
  kdeldsp8 port kdsp8/givel,0.1
  ;
  ; reflection delays
  ;
  ; M1
  kdelsp1m1 port ksp1m1/givel,0.1
  kdelsp2m1 port ksp2m1/givel,0.1
  kdelsp3m1 port ksp3m1/givel,0.1
  kdelsp4m1 port ksp4m1/givel,0.1
  kdelsp5m1 port ksp5m1/givel,0.1
  kdelsp6m1 port ksp6m1/givel,0.1
  kdelsp7m1 port ksp7m1/givel,0.1
  kdelsp8m1 port ksp8m1/givel,0.1
  ; M2
  kdelsp1m2 port ksp1m2/givel,0.1
  kdelsp2m2 port ksp2m2/givel,0.1
  kdelsp3m2 port ksp3m2/givel,0.1
  kdelsp4m2 port ksp4m2/givel,0.1
  kdelsp5m2 port ksp5m2/givel,0.1
  kdelsp6m2 port ksp6m2/givel,0.1
  kdelsp7m2 port ksp7m2/givel,0.1
  kdelsp8m2 port ksp8m2/givel,0.1
  ; M3
  kdelsp1m3 port ksp1m3/givel,0.1
  kdelsp2m3 port ksp2m3/givel,0.1
  kdelsp3m3 port ksp3m3/givel,0.1
  kdelsp4m3 port ksp4m3/givel,0.1
  kdelsp5m3 port ksp5m3/givel,0.1
  kdelsp6m3 port ksp6m3/givel,0.1
  kdelsp7m3 port ksp7m3/givel,0.1
  kdelsp8m3 port ksp8m3/givel,0.1
  ; M4
  kdelsp1m4 port ksp1m4/givel,0.1
  kdelsp2m4 port ksp2m4/givel,0.1
  kdelsp3m4 port ksp3m4/givel,0.1
  kdelsp4m4 port ksp4m4/givel,0.1
  kdelsp5m4 port ksp5m4/givel,0.1
  kdelsp6m4 port ksp6m4/givel,0.1
  kdelsp7m4 port ksp7m4/givel,0.1
  kdelsp8m4 port ksp8m4/givel,0.1

  ;
  ; signal management
  ;
	adeld  delayr   1          ;Path diretto
	adsp1  deltapi  kdeldsp1   ;dir -> sp1
	adsp2  deltapi  kdeldsp2   ;dir -> sp2
	adsp3  deltapi  kdeldsp3   ;dir -> sp3
	adsp4  deltapi  kdeldsp4   ;dir -> sp4
	adsp5  deltapi  kdeldsp5   ;dir -> sp5
	adsp6  deltapi  kdeldsp6   ;dir -> sp6
	adsp7  deltapi  kdeldsp7   ;dir -> sp7
	adsp8  deltapi  kdeldsp8   ;dir -> sp8
	       delayw   asigf       ;filtrato se passa dietro 
;
; enfasi toni medi per il passaggio laterale

	k0 = 0
	kk = 1
	ienfasi = 3  ; 0= enfasi nulla, 1 = regolare
	
	afilt butterbp asig, 3000, 4000
	afilte = afilt*ienfasi
	
	kabsy  = abs(ky)
	kafilt = (kabsy > 1 ? k0 : kk-kabsy)
	kbal   = (kx<0 ? k0 : kk)
	kampl = kafilt*(1-kbal)
	kampr = kafilt*kbal
	al = afilte*kampl     
	ar = afilte*kampr
  ;
  ; path riflessi
  ;
	adelref delayr   1          ;Path riflessi
  ; m1
	asp1m1  deltapi  kdelsp1m1  ;rif m1 -> sp1  
	asp2m1  deltapi  kdelsp2m1  ;rif m1 -> sp2  
	asp3m1  deltapi  kdelsp3m1  ;rif m1 -> sp3
	asp4m1  deltapi  kdelsp4m1  ;rif m1 -> sp4
	asp5m1  deltapi  kdelsp5m1  ;rif m1 -> sp5
	asp6m1  deltapi  kdelsp6m1  ;rif m1 -> sp6
	asp7m1  deltapi  kdelsp7m1  ;rif m1 -> sp7
	asp8m1  deltapi  kdelsp8m1  ;rif m1 -> sp8
  ; m2
	asp1m2  deltapi  kdelsp1m2  ;rif m2 -> sp1  
	asp2m2  deltapi  kdelsp2m2  ;rif m2 -> sp2  
	asp3m2  deltapi  kdelsp3m2  ;rif m2 -> sp3
	asp4m2  deltapi  kdelsp4m2  ;rif m2 -> sp4
	asp5m2  deltapi  kdelsp5m2  ;rif m2 -> sp5
	asp6m2  deltapi  kdelsp6m2  ;rif m2 -> sp6
	asp7m2  deltapi  kdelsp7m2  ;rif m2 -> sp7
	asp8m2  deltapi  kdelsp8m2  ;rif m2 -> sp8
  ; m3
	asp1m3  deltapi  kdelsp1m3  ;rif m3 -> sp1  
	asp2m3  deltapi  kdelsp2m3  ;rif m3 -> sp2  
	asp3m3  deltapi  kdelsp3m3  ;rif m3 -> sp3
	asp4m3  deltapi  kdelsp4m3  ;rif m3 -> sp4
	asp5m3  deltapi  kdelsp5m3  ;rif m3 -> sp5
	asp6m3  deltapi  kdelsp6m3  ;rif m3 -> sp6
	asp7m3  deltapi  kdelsp7m3  ;rif m3 -> sp7
	asp8m3  deltapi  kdelsp8m3  ;rif m3 -> sp8
  ; m4
	asp1m4  deltapi  kdelsp1m4  ;rif m4 -> sp1  
	asp2m4  deltapi  kdelsp2m4  ;rif m4 -> sp2  
	asp3m4  deltapi  kdelsp3m4  ;rif m4 -> sp3
	asp4m4  deltapi  kdelsp4m4  ;rif m4 -> sp4
	asp5m4  deltapi  kdelsp5m4  ;rif m4 -> sp5
	asp6m4  deltapi  kdelsp6m4  ;rif m4 -> sp6
	asp7m4  deltapi  kdelsp7m4  ;rif m4 -> sp7
	asp8m4  deltapi  kdelsp8m4  ;rif m4 -> sp8
	        delayw   asig                
	 
	alpfsp1m3 tone asp1m3, 1311     ;filtro lpf su riflessioni muro 3
	alpfsp2m3 tone asp2m3, 1311     ;20000/1+65%(iymin) 
	alpfsp3m3 tone asp3m3, 1311
	alpfsp4m3 tone asp4m3, 1311
	alpfsp5m3 tone asp5m3, 1311
	alpfsp6m3 tone asp6m3, 1311
	alpfsp7m3 tone asp7m3, 1311
	alpfsp8m3 tone asp8m3, 1311

  asp1 = (((adsp1+al)/kdsp1)+((asp1m1/ksp1m1)+(asp1m2/ksp1m2)+(alpfsp1m3/ksp1m3)+(asp1m4/ksp1m4))*iattroom)*girescale
  asp2 = (((adsp2+ar)/kdsp2)+((asp2m1/ksp2m1)+(asp2m2/ksp2m2)+(alpfsp2m3/ksp2m3)+(asp2m4/ksp2m4))*iattroom)*girescale
  asp3 = (((adsp3+al)/kdsp3)+((asp3m1/ksp3m1)+(asp3m2/ksp3m2)+(alpfsp3m3/ksp3m3)+(asp3m4/ksp3m4))*iattroom)*girescale
  asp4 = (((adsp4+ar)/kdsp4)+((asp4m1/ksp4m1)+(asp4m2/ksp4m2)+(alpfsp4m3/ksp4m3)+(asp4m4/ksp4m4))*iattroom)*girescale
  asp5 = (((adsp5+al)/kdsp5)+((asp5m1/ksp5m1)+(asp5m2/ksp5m2)+(alpfsp5m3/ksp5m3)+(asp5m4/ksp5m4))*iattroom)*girescale
  asp6 = (((adsp6+ar)/kdsp6)+((asp6m1/ksp6m1)+(asp6m2/ksp6m2)+(alpfsp6m3/ksp6m3)+(asp6m4/ksp6m4))*iattroom)*girescale
  asp7 = (((adsp7+al)/kdsp7)+((asp7m1/ksp7m1)+(asp7m2/ksp7m2)+(alpfsp7m3/ksp7m3)+(asp7m4/ksp7m4))*iattroom)*girescale
  asp8 = (((adsp8+ar)/kdsp8)+((asp8m1/ksp8m1)+(asp8m2/ksp8m2)+(alpfsp8m3/ksp8m3)+(asp8m4/ksp8m4))*iattroom)*girescale

	outo	asp1, asp2, asp3, asp4, asp5, asp6, asp7, asp8
         
  garevsp1	=		garevsp1+asp1*iattarev*(1/sqrt(kdsp1)) ;intensita' riverb sp1
  garevsp2	=		garevsp2+asp2*iattarev*(1/sqrt(kdsp2)) ;intensita' riverb sp2
  garevsp3	=		garevsp3+asp3*iattarev*(1/sqrt(kdsp3)) ;intensita' riverb sp3
  garevsp4	=		garevsp4+asp4*iattarev*(1/sqrt(kdsp4)) ;intensita' riverb sp4
  garevsp5	=		garevsp5+asp5*iattarev*(1/sqrt(kdsp5)) ;intensita' riverb sp5
  garevsp6	=		garevsp6+asp6*iattarev*(1/sqrt(kdsp6)) ;intensita' riverb sp6
  garevsp7	=		garevsp7+asp7*iattarev*(1/sqrt(kdsp7)) ;intensita' riverb sp7
  garevsp8	=		garevsp8+asp8*iattarev*(1/sqrt(kdsp8)) ;intensita' riverb sp8

	endin

	          instr 1000 	  ; ***** mandata al riverbero (esterno) *****
iformat     =       8     ; 24-bit file
ifile       =       p4    ; file name

            fout    ifile, iformat, garevsp1, garevsp2, garevsp3, garevsp4, garevsp5, garevsp6, garevsp7, garevsp8
            
        
garevsp1	=			0 
garevsp2	=			0
garevsp3	=			0
garevsp4	=			0
garevsp5	=			0
garevsp6	=			0
garevsp7	=			0
garevsp8	=			0
     
	          endin

            ;
            ; space plot diagnostics
            ;

#define xyreader(chan') #
kxr$chan    zkr     $chan+gixposoff
kyr$chan    zkr     $chan+giyposoff
#

            instr 1001
ksample     timek
ktime       =       ksample/kr
kflag       =       ksample % 200

            $xyreader(1')
            $xyreader(2')
            $xyreader(3')
            $xyreader(4')
            $xyreader(5')
            $xyreader(6')

            if kflag != 0 kgoto finish
            ;                                                    |    1    | |    2    | |    3    | |    4    | |    5    | |    6    | 
            fprintks  "output/plots/space-out-1-6.data", "%9.4f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f\n", ktime, kxr1, kyr1, kxr2, kyr2, kxr3, kyr3, kxr4, kyr4, kxr5, kyr5, kxr6, kyr6
finish:
            endin

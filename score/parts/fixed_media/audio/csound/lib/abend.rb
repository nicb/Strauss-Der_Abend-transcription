
module Abend
  PATH = File.expand_path(File.join(['..'] * 2, 'lib', 'abend'), __FILE__)
end

require File.join(Abend::PATH, 'constants')
require File.join(Abend::PATH, 'system_variables')
require File.join(Abend::PATH, 'note')

module Abend

  class Note
   
    #
    # :index: note number (which turns into instrument etc.)
    # :mat  : musical at (at in bars, fractional number)
    # :note : note in pitch class notation, fractional number
    # :svs  : reference to system variables
    # :mod  : module to use
    #
    attr_reader :index, :mat, :note, :svs
  
    class << self
  
      def include_module(mod)
        include mod
      end
  
    end
  
    def initialize(idx, a, n, sv)
      @index = idx
      @mat = a
      @note = n
      @svs = sv
    end
  
    def at
      self.svs.to_time(self.mat * 4)
    end
  
    def dur
      self.svs.section_duration - self.at
    end
  
    include Abend::Constants
  
    def to_csound
      rel_at = self.svs.section_duration - self.dur + STATIC_POS_SETUP_TIME
      print(";\n; note n.%d\n;\ni%-4d %8.4f %8.4f %+3d %4.2f ; note n.%d\n" % [ self.index, self.index, self.at, self.dur, self.svs.amp, self.note, self.index ])
      print("i%-4d %8.4f %8.4f %5.2f %5.2f ; its static position\n" % [ self.index + SPACE_INIT_OFFSET, 0, STATIC_POS_SETUP_TIME, START_X, START_Y ] )
      print("i%-4d %8.4f %8.4f %8.4f %8.4f %5.2f %8.4f %8.4f ; spiral movement parameters\n" % [ self.index + SPIRAL_MOV_OFFSET, rel_at, self.dur-STATIC_POS_SETUP_TIME,
                                                                                           START_SPEED, END_SPEED, SPIRAL_PHASE, START_DISTANCE, END_DISTANCE ] )
      print("i%-4d %8.4f %8.4f %+d %+d %+d %8.4f %8.4f ; its room\n;\n" % [ self.index + ROOM_INSTRUMENT_OFFSET, 0, self.svs.section_duration,
                                                                         ROOM_ATTENUATION, ROOM_REV_ATT, ROOM_ABSORPTION, START_DISTANCE, END_DISTANCE ])
    end
  
  end

end

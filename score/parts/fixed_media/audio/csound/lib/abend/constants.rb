module Abend

  module Constants
  
    SPACE_INIT_OFFSET      = 100     # space engine initialization instruments (100...)
    STATIC_MOV_OFFSET      = 200     # static movement instruments (200...)
    ROOM_INSTRUMENT_OFFSET = 400     # room instruments
    SPIRAL_MOV_OFFSET      = 300     # spiral movement instruments
    ROOM_ATTENUATION       =  -2
    ROOM_REV_ATT           =  -6
    ROOM_ABSORPTION        =  -6
    STATIC_POS_SETUP_TIME  =  0.01   # time to go into static position
    SPIRAL_PHASE           =  0.25   # phase of the spiral (0.25 == cosine)
  
    module D1
      START_DISTANCE         =  60     # initial radius of the spiral
      END_DISTANCE           =  13     # final radius of the spiral
      START_X                =   0     # initial x position
      START_Y                =  60     # initial y position
      START_SPEED            =  60     # initial rotation speed (in sec)
      END_SPEED              =  30     # final rotation speed (in sec)
    end
  
    module D2
      START_DISTANCE         =  13     # initial radius of the spiral
      END_DISTANCE           =  60     # final radius of the spiral
      START_X                =   0     # initial x position
      START_Y                =  13     # initial y position
      START_SPEED            =  30     # initial rotation speed (in sec)
      END_SPEED              =  60     # final rotation speed (in sec)
    end
  
  end

end

#
# This small piece of code aligns a group of notes towards a final moment
# (which in this specific case ends up being at 120.00)
#
module Abend

  class SystemVariables
  
    attr_accessor :metro, :section_duration, :amp, :reverb_file
  
    def initialize(m = 68, sd = 120, n = '', a = -24)
      self.metro = m
      self.section_duration = sd
      self.amp = a
      self.reverb_file = n
    end
  
    def metro_period
      60.0 / self.metro.to_f
    end
  
    def to_time(r)
      r * self.metro_period
    end
  
    def header
      print <<-EOF.gsub(/^\s+/,'')
        ;
        ; Strauss - Der Abend (1896)
        ; transcription by Nicola Bernardini (2016)
        ;
        ; Fixed media csound score part
        ;
        ; BEWARE! This has been generated with the #{ARGV.first} command line!
        ; Do not edit! Your edits are going to be erased at the next run.
        ; Modify the #{ARGV.first} instead.
        ;
        
        f1 0 32769 11 1
        f2 0 256 7 1 127 1 1 0 128 0
        f3 0 513 7 1 120 1 1 0 391 0
        
        #include "room.geometry"
        
        i1000 0 #{self.section_duration} "#{self.reverb_file}"
        i1001 0 #{self.section_duration} ; space plot diagnostics
      EOF
    end
  
  end
  
end

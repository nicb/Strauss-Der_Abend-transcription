;
; Strauss - Der Abend (1896)
; transcription by Nicola Bernardini (1956)
;
; Fixed media csound orchestra part
;
sr      = 48000
kr      = 24000
nchnls  =     1

gituning = 442.0
gitratio = gituning/cpspch(8.09)


        instr 1, 2, 3, 4, 5, 6
ichan   =        p1
idur    =        p3
itable  =        1
ifreq   =        cpspch(p5) * gitratio
iamp    =        p4
irise   =        20
ifall   =        1
ikpstart=        0.01
ikpend  =        3.99
itrilf  =        6      ; 6 Hz trill
itrila  =        2^(1/12) - 1
igtf    =        0.05
igtfrr  =        0.15

kgtfrr  randh    igtfrr, igtfrr
kgtf    =        igtf + igtfrr + kgtfrr
kgate   oscil    1, kgtf, 3
kamp    =        ampdbfs(iamp + (kgate * 6))    ; rise amp by 6 db on trills
kamp    port     kamp, 0.002
ktrill  oscil    itrila, itrilf, 2
kfreq   =        ifreq * (1 + (ktrill * kgate))
kpow    expon    0.01, idur, 0.5
        prints   "ifreq = %12.8f, ref = %12.8f, ratio = %12.8f, la ref = %12.8f\n", ifreq, cpspch(p5), gitratio, cpspch(8.09)   ; used for debug
;ktime  times
;       fprintks "output/plots/instr-kvalues.data", "%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n", ktime, kgtf, kfreq, kgate, ktrill, kamp ; used for debug
aout    gbuzz    kamp, kfreq, 15, 1, kpow, itable
aout    linen    aout, irise, idur, ifall
        zaw      aout, ichan
        endin

#include "space.orc"

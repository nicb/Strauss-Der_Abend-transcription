# generator 1

require 'abend'

SV = Abend::SystemVariables.new(68, 180, 'output/out-reverb-send_1.wav')

Abend::Note.include_module(Abend::Constants::D1)

nnote = 0

Notes =
[
  Abend::Note.new(nnote += 1, 0.0, 9.07, SV),
  Abend::Note.new(nnote += 1, 2.5, 9.02, SV),
  Abend::Note.new(nnote += 1, 6.5, 9.01, SV),
  Abend::Note.new(nnote += 1, 8.0, 9.03, SV),
  Abend::Note.new(nnote += 1, 23.0, 9.09, SV),
]

SV.header 

Notes.each do
  |n|
  n.to_csound
end

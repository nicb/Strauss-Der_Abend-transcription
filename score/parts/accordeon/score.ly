\version "2.18.2"

\include "include/revision.ly"
\include "include/header.ly"
\include "include/markups.ly"

\paper {
  #(set-paper-size "b4")
}

\include "voices/accordeon.ly"

\header {
  instrument = "accordeon"
}

\score {
  <<
    \score_markups
    \new StaffGroup \with {
      midiInstrument = "accordion"
      instrumentName = "Accordeon"
      \override StaffGrouper.staff-staff-spacing =
          #'((padding . 2)
             (stretchability . 30))
    } <<
      \new Staff \with
      {
        midiMinimumVolume = #0.05
        midiMaximumVolume = #0.85
      } { \accordeon_l }
      \new Staff \with
      {
        midiMinimumVolume = #0.05
        midiMaximumVolume = #0.85
      } { \accordeon_r }
    >>
  >>
  \layout {
    indent = 3\cm
    short-indent = 1\cm
    \context {
       \Score
       \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/20)
        \override Flag.stencil = #modern-straight-flag
    }
     \context {
        \Staff
        \RemoveEmptyStaves
        \override VerticalAxisGroup #'remove-first = ##t
     }
  }
  \midi { 
    \tempo 4 = 63
  }
}

#!/usr/bin/env ruby
#
# Measured accelerando calculator
# for the piano chords from bar 99 to bar 155
#
def calcval(bar, afact, bfact)
  val = afact*bar + bfact
  return val
end

def quarter_rounder(val)
  ires = ((val*400).round / 400.0).to_i
  qres = ((val - ires) * 4).round + 1
  [ires, qres]
end

startbar = 99
endbar   = 154
nbars    = endbar - startbar
nquarters = nbars * 4
endval   = 2
startval = 16

afact = (endval - startval) / nbars.to_f
bfact = startval 

cur = startbar
val = startval
while (cur < endbar)
  (rcur, qcur) = quarter_rounder(cur)
  print("%4d.%1d %8.2f %4d %8.2f\n" % [ rcur, qcur, cur, val, val/4 ])
  cur += (val/4)
  val = calcval(cur-startbar, afact, bfact)
end
